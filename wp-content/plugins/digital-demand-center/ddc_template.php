<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Forge Saas
 */
// HEADER FROM THEME

$parallax = false;
$headerfield = get_field_object('header_options');
$headervalue = $headerfield['value'];

if( have_rows('parallax_field') ) { $parallax = true; }
if( have_rows('parallax_field') || $headervalue == 'Simple Header'  ) { 
	include(dirname( __FILE__ ) . '/simple_header.php'); 
} else { 
	get_header(); 
} ?>



	<?php 
		/* for sites that require full width title banner uncomment this template part and removed title from content area */ 
		//get_template_part('title', 'page'); 
	?>


<?php 
// INCLUDES FOR DDC, JS AND CSS
$plugins_url = plugins_url(); ?>
    <link href="<?php echo $plugins_url; ?>/digital-demand-center/css/ddc-default.css" rel="stylesheet">
    <script src="<?php echo $plugins_url; ?>/digital-demand-center/js/easyResponsiveTabs.js"></script>
    <link href="<?php echo $plugins_url; ?>/digital-demand-center/css/easy-responsive-tabs.css" rel="stylesheet">
    <script src="<?php echo $plugins_url; ?>/digital-demand-center/js/jquery.magnific-popup.js"></script>
    <link href="<?php echo $plugins_url; ?>/digital-demand-center/css/magnific-popup.css" rel="stylesheet">
    <!--Custom CSS-->

	
    <?php the_field('custom_css', 'option'); ?>   


<?php include(dirname( __FILE__ ) . '/parallax_rows.php'); ?>

	<div class="row content-area">

		<div id="content" class="large-9 columns site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				
			<?php include(dirname( __FILE__ ) . '/rows.php'); ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->

	</div>
    <script>
            jQuery(document).ready(function ($) {
                $('.mformbutton').magnificPopup({
                    delegate: 'a',
                    type: 'inline',
                    midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
                });
            });
</script>		
<?php
$footerfield = get_field_object('footer_options');
$footervalue = $footerfield['value'];

if( have_rows('parallax_field') || $footervalue == 'Simple Footer'  ) { 
	include(dirname( __FILE__ ) . '/simple_footer.php'); 
} else { 
	 get_footer(); 
} ?>