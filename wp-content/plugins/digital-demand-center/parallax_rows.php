	<?php if( have_rows('parallax_field') ):
	// loop through the rows of data
		while ( have_rows('parallax_field') ) : the_row();
		$rowcount++;
			if( get_row_layout() == 'parallax_area' ): ?>
			<link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
			<link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
			<style>
			form label {
				font-size: 1em;
				font-weight: 400;
				margin-top: 12px;
			}
			fieldset {
				border: 0;
			}			
			#content {width:100%;padding:1.5em}
			body {font-family:lato;}
			.biggerheadline, h1, h2, h3 {font-family:arvo;}
			.biggerheadline {font-size:48px;line-height:1.1em}
			#parallax-container {
				background-image: url(<?php the_sub_field('parallax_background_image'); ?>);
				display: block;
				background-color: rgba(9,55,94,0.75);
				background-attachment: fixed;
				background-repeat: no-repeat;
				background-position: center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover;
				-ms-behavior: url(backgroundsize.min.htc);
				behavior: url(backgroundsize.min.htc);
				border-style: none;
				margin-left: auto;
				margin-right: auto;
				margin-bottom: 0px;
				width: 100%;
				position: relative;
				border-radius: 0px;
				color:#FFF;
			}
			#overlay {
				background:rgba(9,55,94,0.75);
				position:absolute;
				height:100%;
				width:100%;
			}
			#parallax-content, #parallax-form {
			position:relative;
			z-index:9;
			padding:1.5em;
			}
			#parallax-content {}
			#parallax-form input {background:#CCC}
			.row.content-area {
				background: #fff;
			}
			a.button {
				-webkit-transition: 0.5s ease;
				-moz-transition: 0.5s ease;
				-o-transition: 0.5s ease;
				transition: 0.5s ease;
				font-family: "brandon-grotesque",sans-serif;
				font-style: normal;
				font-weight: 700;
				-webkit-box-shadow: inset 1px 1px 1px rgba(255,255,255,0.35);
				-moz-box-shadow: inset 1px 1px 1px rgba(255,255,255,0.35);
				box-shadow: inset 1px 1px 1px rgba(255,255,255,0.35);
				-webkit-border-radius: 4px;
				-moz-border-radius: 4px;
				border-radius: 4px;
				-moz-background-clip: padding;
				-webkit-background-clip: padding-box;
				background-clip: padding-box;
				color: white;
				cursor: pointer;
				display: inline-block;
				line-height: 1;
				overflow: visible;
				position: relative;
				text-decoration: none;
				text-transform: uppercase;
				font-size: 1em;
				border: 1px solid #000;
				padding: 1em 1.5em;
				background: rgba(247,148,29,1);
			}			
			</style>
			
				<div id="parallax-container">
				<div id="overlay"></div>
					<div class="row">
						<div id="parallax-content" class="large-7 columns"><?php the_sub_field('left_side_content'); ?></div>
						<div id="parallax-form" class="large-5 columns last"><?php $formhtml = get_sub_field('parallax_form'); echo do_shortcode($formhtml); ?></div>
						<div style="clear:both;"></div>
					</div>
				</div>
			<?php endif; ?>	
		<?php endwhile; ?>	
	<?php endif; ?>	
