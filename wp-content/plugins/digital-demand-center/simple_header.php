<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" <?php language_attributes(); ?> > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> > <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
<script src="//use.typekit.net/bjm1sex.js"></script>
<script>try{Typekit.load();}catch(e){}</script>
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

<style>
header.simplemasthead {
	background:#FFF;
	padding:1.5em;
}
.phone-number {text-align:right;line-height:50px;}

@media screen and (max-device-width: 767px) {
    .logo {
        text-align:center;
    }
    .phone-number {
        text-align:center;
        line-height:2em;
    }
}			

</style>

</head>

<body <?php body_class(); ?>>
	<header class="simplemasthead" role="banner">
		<div class="row">
			<div class="logo large-6 med-6 medium-6 columns"><img src="http://www.easymetrics.com/wp-content/uploads/2015/08/simple_logo.png"></div>
			<div class="phone-number  med-6 medium-6  large-6 columns">425-200-0686</div>
		</div>
	</header>