                <?php //START OF TEMPLATES
            $rowcount = 0;
            if( have_rows('content_blocks') ):
            // loop through the rows of data
                while ( have_rows('content_blocks') ) : the_row();
                $rowcount++;
                    if( get_row_layout() == 'general' ): ?>
                        <div id="<?php echo 'row-' . $rowcount; ?>">
                            <h2><?php the_sub_field('section_title'); ?></h2>
                            <div><?php the_sub_field('general_content'); ?></div>
                        </div>
                <?php elseif ( get_row_layout() == 'tabs' ):  ?>
                <?php $mycontent = "";
                while( the_repeater_field('tabs') ) :
                $tabtitle = get_sub_field('tab_title');
                $tabcontent = get_sub_field('tab_content');
                $mytabnav .= '<li>' . $tabtitle . '</li>';
                $mycontent .= '<div>' . $tabcontent . '</div>';       
                endwhile;
                ?>
                <div class="dsbc-section clearall">
                    <div id="dsbc-tab-<?php echo $rowcount; ?>" class="tabgroup" >          
                        <ul class="resp-tabs-list">
                            <?php echo $mytabnav; ?>
                        </ul> 
                        <div class="resp-tabs-container">
                            <?php echo $mycontent; ?>
                        </div>
                    </div>
                </div>

                <script> jQuery( "document" ).ready(function() { 
                    jQuery("#dsbc-tab-<?php echo $rowcount; ?>").easyResponsiveTabs($);
                });   </script>
                
                    <?php elseif ( get_row_layout() == 'youtube_video' ): ?>
                            <?php
                            $field = get_sub_field_object('youtube_video_layout');
                            $value = get_sub_field('youtube_video_layout');
                            $label = $field['choices'][ $value ];
                            $url = get_sub_field('youtube_url');
                            parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                            ?>
                            <div id="<?php echo 'row-' . $rowcount; ?>" class="videoWrapper <?php echo $value ?>">
                                <iframe src="http://www.youtube.com/embed/<?php echo $my_array_of_vars['v'];?>" frameborder="0" allowfullscreen></iframe>
                            </div>
                    <?php elseif ( get_row_layout() == 'vimeo_video' ): ?>
                            <?php
                            $field = get_sub_field_object('vimeo_video_layout');
                            $value = get_sub_field('vimeo_video_layout');
                            $label = $field['choices'][ $value ];
                            $url = get_sub_field('vimeo_url');
                            $vimeoId = explode("/", $url);
                            ?>
                            <div id="<?php echo 'row-' . $rowcount; ?>" class="videoWrapper <?php echo $value ?>">
                                <iframe src="https://player.vimeo.com/video/<?php echo end($vimeoId); ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                    <?php elseif ( get_row_layout() == 'wistia_video' ): ?>
                            <?php
                            $field = get_sub_field_object('wistia_video_layout');
                            $value = get_sub_field('wistia_video_layout');
                            $label = $field['choices'][ $value ];
                            $url = get_sub_field('wistia_url');
                            $urlparts = explode("/", $url);
                            ?>
                            <div id="<?php echo 'row-' . $rowcount; ?>" class="videoWrapper <?php echo $value ?>">
                                <iframe src="//fast.wistia.net/embed/iframe/<?php echo $urlparts['4'];?>?videoFoam=true" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="500" height="281"></iframe><script src="//fast.wistia.net/assets/external/iframe-api-v1.js"></script>
                            </div>
                    <?php elseif ( get_row_layout() == 'lp_form' ): ?>
                            <?php
                            $field = get_sub_field_object('form_layout');
                            $value = get_sub_field('form_layout');
                            $label = $field['choices'][ $value ];
                            $mobfield = get_sub_field_object('mobile_layout');
                            $mobvalue = get_sub_field('mobile_layout');
                            $moblabel = $field['choices'][ $value ];
                            ?>
                            <?php if ( $value == 'left50' ) { ?>
        <style>
            #row-1 {float:right;width:73%;}
            @media only screen and (max-width: 767px) {
                #row-1 {float:none;width:100%;}
            }
        </style>
                            <?php } elseif ( $value == 'right50' ) { ?>
        <style>
            #row-1 {float:left;width:73%;}
            @media only screen and (max-width: 767px) {
                #row-1 {float:none;width:100%;}
            }
        </style>
                            <?php } else { } ?>
                            <?php if($mobvalue == 'inline') : ?>
                            <div id="<?php echo 'row-' . $rowcount; ?>" class="form-wrapper <?php echo $value ?>">
                                <div class="form-content"><?php the_sub_field('form_embed_code'); ?></div>
                            </div>
                            <?php elseif ($mobvalue == 'popup') : ?>
                            <div class="mformbutton"><a href="#<?php echo 'row-' . $rowcount; ?>"><i class="fa fa-paper-plane-o"></i> <span><?php the_sub_field('mobile_form_button_text'); ?></span></a></div>
                            <div id="<?php echo 'row-' . $rowcount; ?>" class="mfp-hide form-wrapper <?php echo $value ?>">
                                <div class="form-content"><?php the_sub_field('form_embed_code'); ?></div>
                            </div>
                            <?php elseif ($mobvalue=='new-page') : ?>
                            <div id="<?php echo 'row-' . $rowcount; ?>" class="form-wrapper <?php echo $value ?>">
                                <div class="form-content"><?php the_sub_field('form_embed_code'); ?></div>
                            </div>
                            <?php endif; ?>
                    <?php elseif ( get_row_layout() == 'webcast' ): ?>
                            <?php
                                $webcastimg = get_sub_field("webcast_image");
                                $buttonURL = get_sub_field("button_link");
                                $buttonText = get_sub_field("button_text");
                                $webcastcontent = '<div style="text-align:center;">
                                    <img style="width:100%;height:auto;" src="' . $webcastimg . '" />
                                    <a href="' . $buttonURL . '"  class="webcast-button">' . $buttonText . '</a>' 
                                    . '</div>';
                                echo do_shortcode('<div id="row-' . $rowcount  .  '" class="webcast-wrapper" >' . $webcastcontent . '</div>' );
                            ?>
                             <?php elseif ( get_row_layout() == 'accordion' ):  ?>
                             <?php $mycontent = "";
                            while(the_repeater_field('ddc_accordion')):
                                $accordiontitle = get_sub_field('accordion_section_title');
                                $field = get_sub_field_object('accordion_icons');
                                $value = get_sub_field('accordion_icons');
                                $icon = $field['choices'][ $value ];  
                                $accordioncontent = get_sub_field('accordion_content');
                                $dsbc_ac_nav .= '<li class="fa fa-' . $value .  '">' . $accordiontitle . '</li>';
                                $mycontent .= '<div>' . $accordioncontent . '</div>';
                            endwhile;
                                ?>
                            <div class="dsbc-section clearall">
                                <div id="ddc-tab-<?php echo $rowcount; ?>" >          
                                    <ul class="resp-tabs-list">
                                        <?php echo $dsbc_ac_nav; ?>
                                    </ul> 
                                    <div class="resp-tabs-container">
                                        <?php echo $mycontent; ?>
                                    </div>
                                </div>
                            </div>
                            <script> 
                                jQuery( document ).ready(function($) { 
                                    $("#ddc-tab-<?php echo $rowcount; ?>").easyResponsiveTabs({ type:"accordion"});                                                
                                });
                            </script>

                    <?php elseif ( get_row_layout() == 'give_me_a_break' ): ?>
                            <?php
                            $field = get_sub_field_object('break_options');
                            $value = get_sub_field('break_options');
                            $label = $field['choices'][ $value ];

                            echo '<div id="row-' . $rowcount . '" class="' . $value . '"></div>';
                    ?>
                    
                    <?php endif; // end row layouts ?>

                    <?php endwhile; // end row layout loop ?>

                    <?php endif; //if have rows ?>