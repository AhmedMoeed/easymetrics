<?php
/*
Plugin Name: Digital Demand Center
Plugin URI: http://gabrielsales.com
Version: Source 1 Purchasing 1.0
Author: Gabriel Sales
Author URI: http://gabrielsales.com
*/

add_action( 'init', 'build_lp_pages' );

//////////////////////// START LANDING PAGE //////////////////////////////////

function build_lp_pages() {

	register_post_type('ddc-landing-page', array(
		'label' => 'Landing Pages',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'page',
		'hierarchical' => false,
		'rewrite' => array(
			"slug" => "lp",
			'with_front' => false,
			'pages' => false),
		'query_var' => true,
		'supports' => array('editor','title','revisions'),
		'taxonomies' => array('ddc-campaign','ddc-buyer-seg','ddc-stage','ddc-format','category'),
		'labels' => array (
			  'name' => 'Digital Demand Center',
			  'singular_name' => 'Landing Page',
			  'menu_name' => 'Digital Demand Center',
			  'add_new' => 'Add Landing Page',
			  'add_new_item' => 'Add New Landing Page',
			  'edit' => 'Edit',
			  'edit_item' => 'Edit Landing Page',
			  'new_item' => 'New Landing Page',
			  'view' => 'View Landing Page',
			  'view_item' => 'View Landing Page',
			  'search_items' => 'Search Landing Pages',
			  'not_found' => 'No Landing Pages Found',
			  'not_found_in_trash' => 'No Landing Pages Found in Trash',
			  'parent' => 'Parent Landing Page'
			)
		)
	);
    register_taxonomy('ddc-campaign',array (
        0 => 'ddc-landing-page',),
        array( 'hierarchical' => true,
        'label' => 'Campaigns',
        'show_ui' => true,'query_var' => true,
        'rewrite' => array('slug' => 'type'),
        'singular_label' => 'Campaign'
        )
    );
	register_taxonomy('ddc-buyer-seg',array (
		0 => 'ddc-landing-page',),
		array( 'hierarchical' => true,
		'label' => 'Buyer Segment',
		'show_ui' => true,'query_var' => true,
		'rewrite' => array('slug' => 'type'),
		'singular_label' => 'Buyer Segment'
		)
    );
	register_taxonomy('ddc-stage',array (
		0 => 'ddc-landing-page',),
		array( 'hierarchical' => true,
		'label' => 'Buyer Education/Stage',
		'show_ui' => true,'query_var' => true,
		'rewrite' => array('slug' => 'type'),
		'singular_label' => 'Buyer Segment'
		)
    );
	register_taxonomy('ddc-format',array (
		0 => 'ddc-landing-page',),
		array( 'hierarchical' => true,
		'label' => 'Format',
		'show_ui' => true,'query_var' => true,
		'rewrite' => array('slug' => 'type'),
		'singular_label' => 'Buyer Segment'
		)
	);
//ADD SETTINGS    
    if ( function_exists( 'acf_add_options_sub_page' ) ){
        acf_add_options_sub_page(array(
            'title'      => 'Settings',
            'parent'     => 'edit.php?post_type=ddc-landing-page',
            'capability' => 'manage_options'
        ));
    }
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_555c929b53ab4',
	'title' => 'DDC Style Settings',
	'fields' => array (
		array (
			'key' => 'field_555c92aca3aee',
			'label' => 'Corner Style',
			'name' => 'corner_style',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'square' => 'square',
				'round' => 'round',
				'pill' => 'pill',
				'custom radius' => 'custom radius',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'horizontal',
		),
		array (
			'key' => 'field_555c990f36be3',
			'label' => 'Custom Radius',
			'name' => 'custom_radius',
			'type' => 'text',
			'instructions' => 'Enter you values with units. example: 5px, 25%, 1em, you can enter values for each corner: 5px 10px 5px 10px (top-left, top-right, bottom-left, bottom-right).',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_555c92aca3aee',
						'operator' => '==',
						'value' => 'custom radius',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_555c93c7a3aef',
			'label' => 'Button & Active Tab Color',
			'name' => 'button_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
		),
		array (
			'key' => 'field_555c944da3af0',
			'label' => 'Button Hover Color',
			'name' => 'button_hover_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
		),
		array (
			'key' => 'field_555c948da3af1',
			'label' => 'Tab Inactive Color',
			'name' => 'tab_inactive_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
		),
		array (
			'key' => 'field_555c9a994c75d',
			'label' => 'Custom CSS',
			'name' => 'custom_css',
			'type' => 'code_area',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'maxlength' => '',
			'rows' => '',
			'code_language' => 'css',
			'code_theme' => 'rubyblue',
			'new_lines' => '',
			'placeholder' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-settings',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;

	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_53e49ac7d4028',
		'title' => 'DDC Landing Page Builder',
		'fields' => array (
			array (
				'key' => 'field_53e49adfc1090',
				'label' => 'Content Blocks',
				'name' => 'content_blocks',
				'prefix' => '',
				'type' => 'flexible_content',
				'instructions' => 'Click the button and select your choice of content, drag and drop to re-order the arrangement.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'button_label' => 'Add a Content Block',
				'min' => '',
				'max' => '',
				'layouts' => array (
					array (
						'key' => '53e4a44eaa464',
						'name' => 'general',
						'label' => 'General (wysiwyg)',
						'display' => 'row',
						'sub_fields' => array (
							array (
								'key' => 'field_53e4a5d224534',
								'label' => 'Section Title (Optional)',
								'name' => 'section_title',
								'prefix' => '',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'readonly' => 0,
								'disabled' => 0,
							),
							array (
								'key' => 'field_53e4a44eaa465',
								'label' => 'WYSIWYG',
								'name' => 'general_content',
								'prefix' => '',
								'type' => 'wysiwyg',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'toolbar' => 'full',
								'media_upload' => 1,
								'tabs' => 'all',
							),
						),
						'min' => '',
						'max' => '',
					),
					array (
						'key' => '53e49af22891b',
						'name' => 'tabs',
						'label' => 'Tabs',
						'display' => 'row',
						'sub_fields' => array (
							array (
								'key' => 'field_53e49b3cc1091',
								'label' => 'Tabs',
								'name' => 'tabs',
								'prefix' => '',
								'type' => 'repeater',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'min' => '',
								'max' => '',
								'layout' => 'table',
								'button_label' => 'Add Tab',
								'sub_fields' => array (
									array (
										'key' => 'field_53e49bb7c1092',
										'label' => 'Tab Title',
										'name' => 'tab_title',
										'prefix' => '',
										'type' => 'text',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array (
											'width' => 20,
											'class' => '',
											'id' => '',
										),
										'default_value' => '',
										'placeholder' => '',
										'prepend' => '',
										'append' => '',
										'maxlength' => '',
										'readonly' => 0,
										'disabled' => 0,
									),
									array (
										'key' => 'field_53e49bccc1093',
										'label' => 'Tab Content',
										'name' => 'tab_content',
										'prefix' => '',
										'type' => 'wysiwyg',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array (
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'default_value' => '',
										'toolbar' => 'full',
										'media_upload' => 1,
										'tabs' => 'all',
									),
								),
							),
						),
						'min' => '',
						'max' => '',
					),
					array (
						'key' => '53e4d200edbb6',
						'name' => 'youtube_video',
						'label' => 'Youtube Video',
						'display' => 'row',
						'sub_fields' => array (
							array (
								'key' => 'field_53e4d655edbb7',
								'label' => 'Video URL',
								'name' => 'youtube_url',
								'prefix' => '',
								'type' => 'text',
								'instructions' => 'ex. https://www.youtube.com/watch?v=84c5KsOnehY',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'readonly' => 0,
								'disabled' => 0,
							),
							array (
								'key' => 'field_53e4d8f95ff78',
								'label' => 'Layout',
								'name' => 'youtube_video_layout',
								'prefix' => '',
								'type' => 'radio',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'fullwidth' => 'Full Width',
									'left50' => '50%, Align Left',
									'right50' => '50%, Align Right',
								),
								'other_choice' => 0,
								'save_other_choice' => 0,
								'default_value' => '',
								'layout' => 'horizontal',
							),
						),
						'min' => '',
						'max' => '',
					),
					array (
						'key' => '53e4d200edbb6b',
						'name' => 'vimeo_video',
						'label' => 'Vimeo Video',
						'display' => 'row',
						'sub_fields' => array (
							array (
								'key' => 'field_53e4d655edbb7b',
								'label' => 'Video URL',
								'name' => 'vimeo_url',
								'prefix' => '',
								'type' => 'text',
								'instructions' => 'ex. https://vimeo.com/130204634',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'readonly' => 0,
								'disabled' => 0,
							),
							array (
								'key' => 'field_53e4d8f95ff78b',
								'label' => 'Layout',
								'name' => 'vimeo_video_layout',
								'prefix' => '',
								'type' => 'radio',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'fullwidth' => 'Full Width',
									'left50' => '50%, Align Left',
									'right50' => '50%, Align Right',
								),
								'other_choice' => 0,
								'save_other_choice' => 0,
								'default_value' => '',
								'layout' => 'horizontal',
							),
						),
						'min' => '',
						'max' => '',
					),
					array (
						'key' => '5424167b11ddf',
						'name' => 'wistia_video',
						'label' => 'Wista Video',
						'display' => 'row',
						'sub_fields' => array (
							array (
								'key' => 'field_5424167b11de0',
								'label' => 'Video URL',
								'name' => 'wistia_url',
								'prefix' => '',
								'type' => 'text',
								'instructions' => 'ex. http://yourdomain.wistia.com/medias/8ko77rff37?embedType=iframe&video',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'readonly' => 0,
								'disabled' => 0,
							),
							array (
								'key' => 'field_5424167b11de1',
								'label' => 'Layout',
								'name' => 'wistia_video_layout',
								'prefix' => '',
								'type' => 'radio',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'fullwidth' => 'Full Width',
									'left50' => '50%, Align Left',
									'right50' => '50%, Align Right',
								),
								'other_choice' => 0,
								'save_other_choice' => 0,
								'default_value' => '',
								'layout' => 'horizontal',
							),
						),
						'min' => '',
						'max' => '',
					),
					array (
						'key' => '53e90768aea40',
						'name' => 'lp_form',
						'label' => 'Contact Form',
						'display' => 'row',
						'sub_fields' => array (
							array (
								'key' => 'field_53e90783aea41',
								'label' => 'Form Embed Code',
								'name' => 'form_embed_code',
								'prefix' => '',
								'type' => 'textarea',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'maxlength' => '',
								'rows' => '',
								'new_lines' => '',
								'readonly' => 0,
								'disabled' => 0,
							),
							array (
								'key' => 'field_53e90fa1ba320',
								'label' => 'Desktop Form Layout',
								'name' => 'form_layout',
								'prefix' => '',
								'type' => 'radio',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'fullwidth' => 'full width',
									'left50' => '25% width,	left aligned',
									'right50' => '25% width right aligned',
								),
								'other_choice' => 0,
								'save_other_choice' => 0,
								'default_value' => '',
								'layout' => 'horizontal',
							),
							array (
								'key' => 'field_53ebb622293c3',
								'label' => 'Mobile Form Layout',
								'name' => 'mobile_layout',
								'prefix' => '',
								'type' => 'radio',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'inline' => 'inline',
									'popup' => 'button w/ pop over',
									'new-page' => 'new page',
								),
								'other_choice' => 0,
								'save_other_choice' => 0,
								'default_value' => '',
								'layout' => 'horizontal',
							),
							array (
								'key' => 'field_53ebb6b6293c4',
								'label' => 'Mobile Form Link',
								'name' => 'mobile_form_link',
								'prefix' => '',
								'type' => 'page_link',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => array (
									array (
										array (
											'field' => 'field_53ebb622293c3',
											'operator' => '==',
											'value' => 'new-page',
										),
									),
								),
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'post_type' => array (
									0 => 'post',
									1 => 'page',
									2 => 'ddc-landing-page',
								),
								'taxonomy' => '',
								'allow_null' => 0,
								'multiple' => 0,
							),
							array (
								'key' => 'field_53ebb4e288854',
								'label' => 'Mobile Form Button Text',
								'name' => 'mobile_form_button_text',
								'prefix' => '',
								'type' => 'text',
								'instructions' => 'If you selected button for mobile display, what should the button text be?',
								'required' => 0,
								'conditional_logic' => array (
									array (
										array (
											'field' => 'field_53ebb622293c3',
											'operator' => '==',
											'value' => 'popup',
										),
									),
									array (
										array (
											'field' => 'field_53ebb622293c3',
											'operator' => '==',
											'value' => 'new-page',
										),
									),
								),
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'readonly' => 0,
								'disabled' => 0,
							),
						),
						'min' => '',
						'max' => '',
					),
					array (
						'key' => '53e91d9843912',
						'name' => 'webcast',
						'label' => 'Webcast (50% Width)',
						'display' => 'row',
						'sub_fields' => array (
							array (
								'key' => 'field_53e91e0e43913',
								'label' => 'Webcast Image',
								'name' => 'webcast_image',
								'prefix' => '',
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'url',
								'preview_size' => 'thumbnail',
								'library' => 'all',
							),
							array (
								'key' => 'field_53e91e4543914',
								'label' => 'Button Link / URL',
								'name' => 'button_link',
								'prefix' => '',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'readonly' => 0,
								'disabled' => 0,
							),
							array (
								'key' => 'field_53e91e7243915',
								'label' => 'Button Text',
								'name' => 'button_text',
								'prefix' => '',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => 'Watch Now',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'readonly' => 0,
								'disabled' => 0,
							),
						),
						'min' => '',
						'max' => '',
					),
					array (
						'key' => '53eb8193a29ab',
						'name' => 'accordion',
						'label' => 'Accordion',
						'display' => 'row',
						'sub_fields' => array (
							array (
								'key' => 'field_53eb8204a29ac',
								'label' => 'Accordion',
								'name' => 'ddc_accordion',
								'prefix' => '',
								'type' => 'repeater',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'min' => '',
								'max' => '',
								'layout' => 'table',
								'button_label' => 'Add Row',
								'sub_fields' => array (
									array (
										'key' => 'field_53eb8226a29ad',
										'label' => 'Accordion Section Title',
										'name' => 'accordion_section_title',
										'prefix' => '',
										'type' => 'text',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array (
											'width' => 15,
											'class' => '',
											'id' => '',
										),
										'default_value' => '',
										'placeholder' => '',
										'prepend' => '',
										'append' => '',
										'maxlength' => '',
										'readonly' => 0,
										'disabled' => 0,
									),
									array (
										'key' => 'field_53eb830ea29af',
										'label' => 'Accordion Content',
										'name' => 'accordion_content',
										'prefix' => '',
										'type' => 'wysiwyg',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array (
											'width' => 70,
											'class' => '',
											'id' => '',
										),
										'default_value' => '',
										'toolbar' => 'full',
										'media_upload' => 1,
										'tabs' => 'all',
									),
								),
							),
						),
						'min' => '',
						'max' => '',
					),
					array (
						'key' => '53fb48fbe135f',
						'name' => 'give_me_a_break',
						'label' => 'Break/Divider',
						'display' => 'row',
						'sub_fields' => array (
							array (
								'key' => 'field_53fb491fe1360',
								'label' => 'Break Options',
								'name' => 'break_options',
								'prefix' => '',
								'type' => 'radio',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'break-only' => 'Break Only',
									'med-break' => 'Break + Space',
									'med-line' => 'Divider Line',
								),
								'other_choice' => 0,
								'save_other_choice' => 0,
								'default_value' => '',
								'layout' => 'horizontal',
							),
						),
						'min' => '',
						'max' => '',
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'ddc-landing-page',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => array (
			0 => 'the_content',
			1 => 'custom_fields',
		),
	));


	endif;
	
	function get_url_contents($url){
			$crl = curl_init();
			$timeout = 5;
			curl_setopt ($crl, CURLOPT_URL,$url);
			curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
			$ret = curl_exec($crl);
			curl_close($crl);
			return $ret;
	};

	// Add specific CSS class by filter
	add_filter('body_class','my_class_names');

	function my_class_names($classes) {
		if (get_field('show_hide_menu') && get_field('show_hide_menu') == "Hide") {
			$classes[] = 'hidenav';
		}
		if (get_field('show_hide_form') && get_field('show_hide_form') == "No, Hide the Form Area") {
			$classes[] = 'nosidebar';
		}
			return $classes;
	}

	function todo_restrict_manage_posts() {
		global $typenow;
		$args=array( 'public' => true, '_builtin' => false );
		$post_types = get_post_types($args);
		if ( in_array($typenow, $post_types) ) {
		$filters = get_object_taxonomies($typenow);
			foreach ($filters as $tax_slug) {
				$tax_obj = get_taxonomy($tax_slug);
				wp_dropdown_categories(array(
					'show_option_all' => __('Show All '.$tax_obj->label ),
					'taxonomy' => $tax_slug,
					'name' => $tax_obj->name,
					'orderby' => 'term_order',
					'selected' => $_GET[$tax_obj->query_var],
					'hierarchical' => $tax_obj->hierarchical,
					'show_count' => false,
					'hide_empty' => true
				));
			}
		}
	}
	function todo_convert_restrict($query) {
		global $pagenow;
		global $typenow;
		if ($pagenow=='edit.php') {
			$filters = get_object_taxonomies($typenow);
			foreach ($filters as $tax_slug) {
				$var = &$query->query_vars[$tax_slug];
				if ( isset($var) ) {
					$term = get_term_by('id',$var,$tax_slug);
					$var = $term->slug;
				}
			}
		}
		return $query;
	}
	add_action( 'restrict_manage_posts', 'todo_restrict_manage_posts' );
	add_filter('parse_query','todo_convert_restrict');

	function add_my_salesletters( $pages ) {
		 $my_salesletters_pages = new WP_Query( array( 'post_type' => 'ddc-landing-page' ) );
		 if ( $my_salesletters_pages->post_count > 0 )
		 {
			 $pages = array_merge( $pages, $my_salesletters_pages->posts );
		 }
		 return $pages;
	}
	add_filter( 'get_pages',  'add_my_salesletters' );

	// End Home Page Filter

	function enable_front_page_stacks( $query ){
	  if('' == $query->query_vars['post_type'] && 0 != $query->query_vars['page_id'])
			$query->query_vars['post_type'] = array( 'ddc-landing-page','page');
	}
	add_action( 'pre_get_posts', 'enable_front_page_stacks' );

	// USE CUSTOM PAGE TEMPLATE
	function get_custom_post_type_template($single_template) {
		 global $post;

		 if ($post->post_type == 'ddc-landing-page') {
			  $single_template = dirname( __FILE__ ) . '/ddc_template.php';
		 }
		 return $single_template;
	}
	add_filter( 'single_template', 'get_custom_post_type_template' );

	// Add icon to admin

	add_action( 'admin_head', 'cpt_icons' );

	function cpt_icons() {
		?>
		<style type="text/css" media="screen">
			#menu-posts-ddc-landing-page .wp-menu-image {
				background: url(<?php echo plugins_url( 'gs-icon.png' , __FILE__ ); ?>) no-repeat 0 -32px !important;
			}
			#menu-posts-ddc-landing-page:hover .wp-menu-image, #menu-posts-ddc-landing-page.wp-has-current-submenu .wp-menu-image {
				background-position: 0 0 !important;
			}
			#adminmenu  #menu-posts-ddc-landing-page div.wp-menu-image:before {
				content: "";
			}
		</style>
	
	<?php 

	} 
}

function ddc_flush_rewrites() {
	// call your CPT registration function here (it should also be hooked into 'init')
	build_lp_pages();
	flush_rewrite_rules();
}

register_activation_hook( __FILE__, 'ddc_flush_rewrites' );

function ddc_flush_rewrites_deactivate() {
	flush_rewrite_rules();
}

register_deactivation_hook( __FILE__, 'ddc_flush_rewrites_deactivate' );

//////////////////////// END LANDING PAGE ////////////////////////////////// ?>