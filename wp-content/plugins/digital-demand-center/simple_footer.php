<style>
footer[role=contentinfo] {
    margin-top: 0;
    padding: 2em;
    background-color: #444;
    color: #fff;
    font-size: 24px;
    line-height: 1.5em;
}
</style>
	<footer id="colophon" class="site-footer panel" role="contentinfo">

			<div class="row">

				<div class="large-12 columns">
					<div style="text-align:center;">
						CONTACT US<br />
						sales@easymetrics.com<br />
						425-200-0686<br />
					</div>
				</div>

			</div>
	</footer>

<?php wp_footer(); ?>
<script>
	jQuery(document).foundation();
</script>
</body>
</html>