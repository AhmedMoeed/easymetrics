<?php
/**
 * template name: temp-faq
 */

get_header();
?>

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/inner-header'); ?>
<!-- Call to Action Section END -->

<div class="innerpage_wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 main_left_side">

				<?php the_content(); ?>
				<?php if ( have_rows( 'faq' ) ) : ?>
					<div class="section-container accordion">

						<?php while(have_rows('faq')) : the_row(); ?>
							
							<?php if ( get_sub_field( 'topic' ) ) : ?>
								<h4 class="mb-3"><?php the_sub_field( 'topic' ); ?></h4>
							<?php endif; ?>

							<div class="faq-section">
								<?php while ( have_rows('questions') ) : the_row(); ?>
								<section class="faq-block">
									<p class="title">
										<span class="question">Q.</span>
										<?php the_sub_field('question'); ?>
									</p>
									<div class="content">
										<?php the_sub_field('answer'); ?>
									</div>
								</section>
								<?php endwhile; ?>
							</div>

						<?php endwhile; ?>

					</div>
				<?php endif; ?>

			</div> <!-- col 8 -->
			<div class="col-lg-4 main_right_side">
				<?php get_sidebar(); ?>
			</div> <!-- col 4 -->
		</div> <!-- row -->
	</div> <!-- container -->
</div>

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action'); ?>
<!-- Call to Action Section END -->

<?php get_footer();
