<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package EasyMetrics
 */

get_header();
?>
<div class="innerpage_wrap">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 col-lg-8">

				<?php if ( have_posts() ) : ?>

					<header class="page-header">
						<?php
						the_archive_title( '<h3 class="page-title">', '</h3>' );
						the_archive_description( '<div class="archive-description">', '</div>' );
						?>
					</header><!-- .page-header -->
					<hr>
					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/*
						* Include the Post-Type-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Type name) and that will be used instead.
						*/
						get_template_part( 'template-parts/content', get_post_type() );

					endwhile;

					the_posts_navigation();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>

			</div> <!-- col 8 -->	
			<div class="col-lg-4">
				<?php get_sidebar('blog'); ?>
			</div> <!-- col 4 -->
		</div> <!-- row -->
	</div> <!-- container -->
</div>  <!-- innerpage_wrap -->

<?php
get_footer();
