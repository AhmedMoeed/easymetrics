<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EasyMetrics
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="site-header">
	<div class="container d-flex align-items-center">
		
		<div class="logo-wrap">
			<a href="<?= esc_url( home_url( '/' ) ); ?>"  class="logo">
				<?php 
					$logo_desktop = get_field('logo_desktop', 'option');
					if( !empty($logo_desktop) ): ?>
						<img src="<?= $logo_desktop['url']; ?>" 
						alt="<?= $logo_desktop['alt']; ?>" 
						class="desktop"
						/>
				<?php endif; ?>

				<?php 
					$logo_mobile = get_field('logo_mobile', 'option');
					if( !empty($logo_mobile) ): ?>
						<img 
						src="<?= $logo_mobile['url']; ?>" 
						alt="<?= $logo_mobile['alt']; ?>" 
						class="mobile"
						/>
				<?php endif; ?>
			</a>
		</div>
		
		<div class="ml-auto d-flex align-items-center">
			<div class="main-nav-wrap">
				<nav>
					<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-header',
							'menu_id'        => 'primary-menu',
							'menu_class'     => 'main-nav',
							'container'		 => '',
							'container_class' => ''
						) );
					?>
				</nav>
			</div>
			<div class="get-started-btn d-flex ">
				<a href="/easymetrics/request-a-custom-demo/" class="get_started"> Get Started <i class="arrow-right"></i></a>
				<a href="https://app.gcp-easymetrics.com/index.php" class="login-menu-item d-none d-lg-block"> Log In </a>
			</div>
			<div class="mobile-button">
				<span class="line-one"></span>
				<span class="line-two"></span>
				<span class="line-three"></span>
			</div>
		</div>
		
	</div> <!-- container -->
</header> <!--Header End-->

<div class="header-gap"></div>