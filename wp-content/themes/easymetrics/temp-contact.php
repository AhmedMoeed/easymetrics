<?php
/**
 * template name: temp-contact
 */

get_header();
?>

<div class="inner-page-header">
	<div class="container">
		<p class="text-skyBlue mb-0">Got a question? </p>
		<?php the_title( '<h1>', '</h1>' ); ?>
	</div> <!-- container -->
</div> <!-- inner page header -->

<div class="innerpage_wrap">	
	<div class="container">
		<div class="contactpage-wrap">
			
			<div class="row">
				<div class="col-lg-4">
					<div>
						<div class="text-content">
							<?php  the_content(); ?>
						</div>
					</div>
					<?php get_template_part( 'template-parts/contact-info'); ?>
				</div>
				<div class="col-lg-8">
					<div class="requestdemo-form mt-0">
						<?php echo do_shortcode('[contact-form-7 id="2479" title="ContactUs Form"]'); ?>
					</div>
				</div>
			</div> <!-- row -->
		</div> <!-- contactpage-wrap -->
	</div> <!-- container -->
</div> <!-- innerwrap -->

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action', 'page' ); ?>
<!-- Call to Action Section END -->

<?php get_footer();
