<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EasyMetrics
 */

?>

<div class="sidebar">

    <div class="search-widget">
        <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
            <input type="text" name="s" id="search" class="form-control" placeholder="Search..." value="<?php the_search_query(); ?>" />
        </form>
    </div>

    <?php
        $i = 0;

        $args = array( 
            'post_type' 		=> 'post', 
            'posts_per_page' 	=> -1, 
            'post_status'		=> 'publish',
            'meta_query' => array(
                array(
                    'key'     => 'make_featured_post', 
                    'value'   => true
                ),
            ),
        );

        $postslist = new WP_Query( $args );
        if ( $postslist->have_posts() ) : ?>
            <div class="widget">
                <h4 class="widget-title">Featured</h4>
                <div class="text-content">
                    <ul class="list-items featured-blogs">
                        <?php 
                        while ( $postslist->have_posts() ) : $postslist->the_post();
                        ?>
                            <li>
                                <a href="<?php the_permalink() ?>">
                                    <?php echo the_title(); ?>
                                </a>
                            </li> 
                        <?php  endwhile; 
                        wp_reset_postdata();?>
                    </ul>
                </div>
            </div>  <!-- widget Featured Posts -->
    <?php endif; ?>


    <div class="widget">
        <h4 class="widget-title">Topics</h4>
        <div class="text-content">
            <ul class="list-items featured-blogs">
                <?php wp_list_categories( array(
                    'orderby'       => 'name',
                    'hierarchical'  => false,
                    'title_li'      => ''
                ) ); ?> 
            </ul>
        </div>
    </div>  <!-- widget Featured Posts -->

    <div class="widget subscription-widget">
        <h4 class="widget-title">Let's stay connected </h4>
        <div class="divider">
            <span class="line"></span>
        </div>
        <p>Sign up now to recieve great content</p>
        <div class="text-content">
            <div class="subscriptionform">
                <?php echo do_shortcode('[contact-form-7 id="2490" title="Subscribe-Form"]'); ?>
            </div>
        </div>
    </div>  <!-- widget Featured Posts -->

</div>
