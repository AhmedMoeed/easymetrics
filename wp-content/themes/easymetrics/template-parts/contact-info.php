<div class="footer-contact-info">
	<div class="social-media">
		<?php
			$twitter_url = get_field('twitter_url', 'option');
			if( !empty($twitter_url) ): ?>
				<a href="<?= $twitter_url; ?>" target="_blank">
					<img src="<?= get_template_directory_uri();?>/assets/images/icons/twitter.png" alt="Twitter"/>
				</a>
		<?php endif ?>

		<?php
			$facebook_url = get_field('facebook_url', 'option');
			if( !empty($facebook_url) ): ?>
				<a href="<?= $facebook_url; ?>" target="_blank">
				<img src="<?= get_template_directory_uri();?>/assets/images/icons/facebook.png" alt="Facebook"/>
			</a>
		<?php endif ?>

		<?php
			$linked_in_url = get_field('linked_in_url', 'option');
			if( !empty($linked_in_url) ): ?>
				<a href="<?= $linked_in_url; ?>" target="_blank">
				<img src="<?= get_template_directory_uri();?>/assets/images/icons/linkedIn.png" alt="Linked IN"/>
			</a>
		<?php endif ?>
	</div>

	<?php
		$phone = get_field('phone', 'option', false, false);
		if( !empty($phone) ) { echo 'US : ' . $phone . "<br/>"; }
	?>
	<?php
		$email_sales = get_field('email_sales', 'option');
		if( !empty($email_sales) ) {
			echo '<a href="mailto:'.$email_sales.'">'.$email_sales.'</a><br />';
		}
	?>
	<?php
		$email_support = get_field('email_support', 'option');
		if( !empty($email_support) ) {
			echo '<a href="mailto:'.$email_support.'">'.$email_support.'</a><br/><br/>';
		}
	?>
	
	<address>
		<?php
			$address = get_field('address', 'option', false, false);
			if( !empty($address) ) {
				echo $address;
			}
		?>
	</address>
</div>