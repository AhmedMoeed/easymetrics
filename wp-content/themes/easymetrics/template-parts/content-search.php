<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package EasyMetrics
 */

?>

<div class="col-md-6">
	<div class="blog-post">
		<a href="<?php the_permalink() ?>" class="link"></a>
		<?php echo get_the_post_thumbnail(); ?>
		<div class="blog-content">
			<div class="category">
				<?= get_category($post_categories[0])->name; ?>
			</div>
			<div class="text-content">
				<h4><?php the_title(); ?></h4>
			</div>
			<div class="meta-info">
				<?php 
					echo get_the_date( 'M j, Y' ); 
					$author = get_the_author_meta('display_name');
					if($author) { echo ' |  By : ' . $author; }
				?>
			</div>
		</div>
	</div>
</div> <!-- col 6 -->