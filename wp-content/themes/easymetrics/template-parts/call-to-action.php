<?php if( is_front_page() ) {
	echo '<section class="footer_cta cta_overlap">';
} else {
	echo '<section class="footer_cta">';
}
?>
	<div class="container">
		<?php
			$title = get_field('ca_title', 'option');
			if( !empty($title) ) {
				echo '<h2>'.$title.'</h2>';
			}
		?>
		<?php
			$ca_text_content = get_field('ca_text_content', 'option');
			if( !empty($ca_text_content) ) { ?>
				<div class="text-content">
					<?php echo $ca_text_content; ?>
				</div>
		<?php
			}
		?>

		<?php
			$ca_button_text = get_field('ca_button_text', 'option');
			$ca_button_URL = get_field('ca_button_url', 'option');
			
			if( !empty($title) ) { ?>
				<a href="<?= $ca_button_URL; ?>" class="btn btn-primary">
					<?= $ca_button_text; ?>
				</a>
		<?php } ?>
		
	</div> <!-- container -->
</section> <!-- section -->
