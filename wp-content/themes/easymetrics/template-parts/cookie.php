<?php 
$cookie_text = get_field('cookie_text', 'option');
if( !empty($cookie_text) ): ?>

	<div class="cookies-accept">
		<?= $cookie_text; ?>
		<span class="cookies-accept-btn yes">OK</span>
		<!-- <span class="cookies-reject-btn">No</span> -->
	</div> <!-- cookies bar -->
<?php endif; ?>

<!-- The Premier League website employs cookies to improve your user experience. We have updated our cookie policy to reflect changes in the law on cookies and tracking technologies used on websites. If you continue on this website, you will be providing your consent to our use of cookies. -->
