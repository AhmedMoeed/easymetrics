<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EasyMetrics
 */

?>

<footer class="site-footer">
	<div class="container">
		<div class="mb-4 d-none d-xl-block">
			<?php 
				$logo_footer = get_field('logo_footer', 'option');
				if( !empty($logo_footer) ): ?>
					<img src="<?= $logo_footer['url']; ?>" 
					alt="<?= $logo_footer['alt']; ?>" 
					class="logo-sm"
				/>
			<?php endif; ?>
		</div>
		<div class="row">
			<div class="col-xl-10 col-md-12 border-bottom-blue">
				<div class="row">
					<div class="col-lg-3">
						<div class="footer-widget">
							<div class="widget-title">
								<h5>Platform Features & Benefits</h5>
								<?php
									wp_nav_menu( array(
										'theme_location' 	=> 'menu-footer1',
										'menu_class' 		=> '',
										'menu_id' 			=> '',
										'container'	=> ''
									) );
								?>
							</div>
						</div>
					</div> <!-- col 3 -->
					<div class="col-lg-3">
						<div class="footer-widget">
							<div class="widget-title">
								<h5>Solutions</h5>
								<?php
									wp_nav_menu( array(
										'theme_location' => 'menu-footer2',
										'menu_class' 		=> '',
										'menu_id' 			=> '',
										'container'	=> ''
									) );
								?>
							</div>
						</div>
					</div> <!-- col 3 -->
					<div class="col-lg-3">
						<div class="footer-widget">
							<div class="widget-title">
								<h5>Blog Topics</h5>
								<?php
									wp_nav_menu( array(
										'theme_location' => 'menu-footer3',
										'menu_class' 		=> '',
										'menu_id' 			=> '',
										'container'	=> ''
									) );
								?>
							</div>
						</div>
					</div> <!-- col 3 -->
					<div class="col-lg-3">
						<div class="footer-widget">
							<div class="widget-title">
								<h5>About</h5>
								<?php
									wp_nav_menu( array(
										'theme_location' => 'menu-footer4',
										'menu_class' 		=> '',
										'menu_id' 			=> '',
										'container'	=> ''
									) );
								?>
							</div>
						</div>
					</div> <!-- col 3 -->
				</div> <!-- row -->
			</div> <!-- col 3 -->
			
			<div class="col-xl-2 border-bottom-blue border-lg-bottom-none">
				<div class="footer-widget border-lg-top-none">
					<?php get_template_part( 'template-parts/contact-info'); ?>
				</div>
			</div> <!-- col 3 -->
		</div> <!-- row -->

	</div> <!-- container -->

	<div class="copyright">
		<div class="container">
			<div class="mb-3 d-xl-none">
				<?php 
					$logo_footer = get_field('logo_footer', 'option');
					if( !empty($logo_footer) ): ?>
						<img src="<?= $logo_footer['url']; ?>" 
						alt="<?= $logo_footer['alt']; ?>" 
						class="logo-sm"
						/>
				<?php endif; ?>
			</div>
			<div class="d-block d-sm-flex justify-content-center justify-content-xl-start">
				<?php 
					$copy_right_text = get_field('copy_right_text', 'option');
					if( !empty($copy_right_text) ) {
						echo $copy_right_text;	
					}
				?>
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-copyright',
						'menu_class' 		=> 'd-flex justify-content-center justify-content-sm-start list-inline copy-right-menu ml-3 mb-0',
						'menu_id' 			=> '',
						'container'	=> ''
						) );
				?>
			</div>
		</div> <!-- container -->
	</div> <!-- copyright -->

</footer> <!-- footer -->
<?php get_template_part( 'template-parts/cookie'); ?>
<?php wp_footer(); ?>

</body>
</html>
