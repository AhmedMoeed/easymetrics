<?php
/**
 * template name: temp-partners
 */

get_header();
?>

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/inner-header', 'page' ); ?>
<!-- Call to Action Section END -->

<div class="innerpage_wrap">
	<div class="container"> 
			<?php
			$custom_terms = get_terms('partner-types');
			$i = 1;
			foreach($custom_terms as $custom_term) {
				wp_reset_query();
				$args = array(
					'post_type' => 'partners',
					'order' => 'DESC',
					'posts_per_page' 	=> -1,
					'post_status'		=> 'publish',
					'tax_query' => array(
						array(
							'taxonomy' => 'partner-types',
							'field' => 'slug',
							'terms' => $custom_term->slug,
						),
					),
				);

				$loop = new WP_Query($args);
				if($loop->have_posts()) { 
					?>
					<div class="partner-section mb-5">
						<div class="mb-4">
							<h3><?= $custom_term->name; ?></h3>
						</div>
						<div class="row">
							<?php while($loop->have_posts()) : $loop->the_post(); ?>
								<div class="col-md-4 col-sm-6">
									<div class="partner-box">
										<a href="<?php echo get_permalink(); ?>" data-toggle="modal" data-target="#leader<?=$i;?>">
											<?php echo get_the_post_thumbnail();?>
											<?php get_the_title(); ?>
										</a>
									</div>
								</div>
								<div class="modal fade leaderPopup" id="leader<?=$i;?>" tabindex="-1" role="dialog" aria-hidden="true">
								  <div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
									  <div class="modal-body">
										<?php echo the_content(); ?>
									  </div>
									</div>
								  </div>
								</div>
							<?php 
							$i++;
							endwhile;
							wp_reset_postdata();
							?>
						</div>
					</div>
					<?php 
				}
			}

			?>
	</div> <!-- container -->
</div> <!-- innerpage_wrap -->

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action', 'page' ); ?>
<!-- Call to Action Section END -->

<?php get_footer();
