<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package EasyMetrics
 */

get_header();
?>

<?php while ( have_posts() ) : the_post(); ?>

	<section class="hero blog-detail-hero overlapHero">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-12">
					<h1 class="text-blue"><?php echo get_the_title(); ?></h1>
					<div class="meta-info">
						<?php 
							if(is_singular( 'post' )) {
								echo 'Posted On : ' . get_the_date( 'M j, Y' ); 	
							}
						?>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="text-content">
							</div>
						</div>
					</div>
				</div> <!-- col -->
			</div> <!-- row -->
		</div> <!-- container -->
	</section> <!-- section End -->

	<section class="blog-body overlapTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 main_left_side">
					<?php
						if ( has_post_thumbnail() ) {
							echo '<div class="blog-featured-image">';
							easymetrics_post_thumbnail();
							echo '</div>';
						}
					?>
					<div class="blog-content single-page-body">
						<div class="page-main-content-area">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<div class="col-lg-4 main_right_side">
					<?php get_sidebar(); ?>
				</div> <!-- sidebar -->
			</div> <!-- row -->
		</div> <!-- container -->
	</section> <!-- section End -->

<?php endwhile; ?>

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action'); ?>
<!-- Call to Action Section END -->

<?php get_footer();
