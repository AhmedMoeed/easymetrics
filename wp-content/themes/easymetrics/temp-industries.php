<?php
/**
 * template name: industries
 */

get_header();
?>

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/inner-header', 'page' ); ?>
<!-- Call to Action Section END -->

<div class="innerpage_wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 main_left_side">
				<?php
					$i = 1;
					$people_args = array(
						'posts_per_page' => -1, 
						'post_type' => 'industries'
					); 
					$people = new WP_Query($people_args);
					while ( $people->have_posts() ) : $people->the_post(); 
					// $image = get_the_post_thumbnail_url();
					$icon = get_field('industry_thumbnail_icon');
				?>
					<div class="industry-item">
						<div class="row align-items-center">
							<div class="col-md-3 col-sm-4">
								<span class="icon">
								<?php
								if( !empty($icon) ): ?>
									<img src="<?php echo $icon['url']; ?>" 
									alt="<?php echo $icon['alt']; ?>" />
								<?php endif; ?>
								</span>
							</div>
							<div class="col-md-9 col-sm-8">
								<h2><?php the_title(); ?></h2>
								<div class="text-content">
									<p><?php echo get_the_excerpt(); ?></p>
									<a href="<?php the_permalink(); ?>">Learn more about <?php the_title(); ?> -></a>
								</div>
							</div>
						</div>
					</div><!-- industry -->

				<?php 
				$i++;
				endwhile; // end of the loop. ?>
			</div> <!-- col 8 -->
			<div class="col-lg-4 main_right_side">
				<?php get_sidebar(); ?>
			</div> <!-- col 4 -->
		</div><!-- row -->
	</div> <!-- container -->
</div> <!-- innerpage_wrap -->

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action', 'page' ); ?>
<!-- Call to Action Section END -->

<?php get_footer();
