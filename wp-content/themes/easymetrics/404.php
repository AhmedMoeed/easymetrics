<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package EasyMetrics
 */

get_header();
?>

<section class="wrap404">
	<div class="container text-center">
		<div class="inner-content d-flex justify-content-center align-items-center">
			<div>
				<?php
					$title = get_field('404_title', 'option');
					if( !empty($title) ) {
						echo '<h2>'. $title .'</h2>';
					}
				?>
				<?php
					$textContent = get_field('404_text_content', 'option');
					if( !empty($textContent) ) {
						echo '<p>'.$textContent.'</p>';
					}
				?>
				<?php
					$button_text = get_field('404_btn_text', 'option');
					
					if( !empty($button_text) ) { ?>
						<a href="<?= esc_url( home_url( '/' ) ); ?>" class="mt-3 btn btn-primary">
							<?= $button_text; ?>
						</a>
				<?php } ?>
			</div>
		</div>
	</div> <!-- container -->
</section> <!-- section -->

<?php get_footer();
