<?php
/**
 * template name: temp-products
 */

get_header();
?>

<section class="hero products-hero">
	<div class="container">
		<div class="row flex-row-reverse align-items-center">
			<div class="col-xl-6 col-md-6 offset-xl-1">
                <?php 
					$sec1_image = get_field('sec1_image');
					if( !empty($sec1_image) ): ?>
						<div class="hero-img">
							<img src="<?= $sec1_image['url']; ?>"  alt="<?= $sec1_image['alt']; ?>" />
						</div>
				<?php endif; ?>
			</div>
			<div class="col-xl-5 col-md-6 col-sm-12">
                <?php
					$sec1_title = get_field('sec1_title');
					if( !empty($sec1_title) ) {
						echo '<h2>'. $sec1_title .'</h2>';
					}
                ?>
                <?php
					$sec1_btn_text = get_field('sec1_btn_text');
					$sec1_btn_url = get_field('sec1_btn_url');
					if( !empty($sec1_btn_text) ) { ?>
						<a href="<?= $sec1_btn_url; ?>" class="btn btn-primary mt-4"> <?= $sec1_btn_text; ?> </a>
				<?php } ?>
			</div> <!-- col -->
		</div><!-- row -->
	</div> <!-- container -->
</section> <!-- section -->


<div class="innerpage_wrap">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 mx-auto">
                <div class="text-center">
                    <?php
                        $sec2_title = get_field('sec2_title');
                        if( !empty($sec2_title) ) {
                            echo '<h2>'. $sec2_title .'</h2>';
                        }
                    ?>
                    <?php
                        $sec2_subtitle = get_field('sec2_subtitle');
                        if( !empty($sec2_subtitle) ) {
                            echo '<p>'. $sec2_subtitle .'</p>';
                        }
                    ?>
                </div>

                <?php if( have_rows('sec2_carousel') ): 
                    $i = 0;
                    ?>
                    <div id="carouselExampleControls" class="product_carousel carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php while( have_rows('sec2_carousel') ): the_row(); 
                                $sec2_image = get_sub_field('sec2_image');
                                $sec2_text_left = get_sub_field('sec2_text_left');
                                $sec2_text_middle = get_sub_field('sec2_text_middle');
                                $sec2_text_right = get_sub_field('sec2_text_right');
                            ?>
                                <?php if($i===0) {?>
                                    <div class="carousel-item active">
                                <?php } else { ?>
                                        <div class="carousel-item">
                                <?php } ?>
                                    <?php if( $sec2_image ): ?>
                                        <div class="carousel-image">
                                            <img src="<?php echo $sec2_image['url']; ?>" alt="<?php echo $sec2_image['alt'] ?>" />
                                        </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <?php
                                            if( !empty($sec2_text_left) ) { ?>
                                                <div class="col-md-4">
                                                    <div class="c-border-top">
                                                        <?= $sec2_text_left ?>
                                                    </div>
                                                </div> <!-- col 4 -->
                                        <?php } ?>
                                        <?php
                                            if( !empty($sec2_text_middle) ) { ?>
                                                <div class="col-md-4">
                                                    <div class="c-border-top">
                                                        <?= $sec2_text_middle ?>
                                                    </div>
                                                </div> <!-- col 4 -->
                                        <?php } ?>
                                        <?php
                                            if( !empty($sec2_text_right) ) { ?>
                                                <div class="col-md-4">
                                                    <div class="c-border-top">
                                                        <?= $sec2_text_right ?>
                                                    </div>
                                                </div> <!-- col 4 -->
                                        <?php } ?>

                                    </div> <!-- row -->
                                </div>
                            <?php 
                            $i++;
                            endwhile; ?>
                        </div> <!-- row --> 
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <i class="fas fa-chevron-left"></i>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </div> <!-- row -->
                <?php endif; ?>        

            </div> <!-- col 9 -->
        </div> <!-- row -->                
    </div> <!-- container -->
</div> <!-- inner page wrap -->


<div class="innerpage_wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-10 mx-auto">
                <div class="row align-items-center">
                    <div class="col-sm-5 mb-4 mb-sm-0">
                        <?php 
                            $sec3_image = get_field('sec3_image');
                            if( !empty($sec3_image) ): ?>
                                <img src="<?= $sec3_image['url']; ?>"  alt="<?= $sec3_image['alt']; ?>"
                                    class="shadow-img"
                                    />
                        <?php endif; ?>
                    </div> <!-- col -->
                    <div class="col-sm-7">
                        <div class="pb-lg-4 mb-lg-5">
                            <?php
                                $sec3_title = get_field('sec3_title');
                                if( !empty($sec3_title) ) {
                                    echo '<h2>'. $sec3_title .'</h2>';
                                }
                            ?>
                            <?php
                                $sec3_subtitle = get_field('sec3_subtitle');
                                if( !empty($sec3_subtitle) ) {
                                    echo '<p>'. $sec3_subtitle .'</p>';
                                }
                            ?>
                        </div>

                        <div class="row">
                            <?php
                                $sec3_text_1 = get_field('sec3_text_1');
                                if( !empty($sec3_text_1) ) { ?>
                                    <div class="col-lg-6 col-md-12 mb-lg-5">
                                        <div class="c-border-top f-md">
                                            <?= $sec3_text_1 ?>
                                        </div>
                                    </div> <!-- col -->
                            <?php } ?>
                            <?php
                                $sec3_text_2 = get_field('sec3_text_2');
                                if( !empty($sec3_text_2) ) { ?>
                                    <div class="col-lg-6 col-md-12 mb-lg-5">
                                        <div class="c-border-top f-md">
                                            <?= $sec3_text_2 ?>
                                        </div>
                                    </div> <!-- col -->
                            <?php } ?>
                            <?php
                                $sec3_text_3 = get_field('sec3_text_3');
                                if( !empty($sec3_text_3) ) { ?>
                                    <div class="col-lg-6 col-md-12 mb-lg-5">
                                        <div class="c-border-top f-md">
                                            <?= $sec3_text_3 ?>
                                        </div>
                                    </div> <!-- col -->
                            <?php } ?>
                            <?php
                                $sec3_text_4 = get_field('sec3_text_4');
                                if( !empty($sec3_text_4) ) { ?>
                                    <div class="col-lg-6 col-md-12 mb-lg-5">
                                        <div class="c-border-top f-md">
                                            <?= $sec3_text_4 ?>
                                        </div>
                                    </div> <!-- col -->
                            <?php } ?>

                        </div> <!-- row -->
                    </div>  <!-- col -->
                </div> <!-- row -->
            </div> <!-- col -->
        </div> <!-- row -->   
        
        <div class="row mt-5">
            <div class="col-md-10 mx-auto">
                <div class="row flex-row-reverse align-items-center">
                    <div class="col-sm-5 mb-4 mb-sm-0">
                        <?php 
                            $sec4_image = get_field('sec4_image');
                            if( !empty($sec4_image) ): ?>
                                <img src="<?= $sec4_image['url']; ?>"  alt="<?= $sec4_image['alt']; ?>"
                                    class="shadow-img"
                                    />
                        <?php endif; ?>
                    </div> <!-- col -->
                    <div class="col-sm-7">
                        <div class="pb-lg-4 mb-lg-5">
                            <?php
                                $sec4_title = get_field('sec4_title');
                                if( !empty($sec4_title) ) {
                                    echo '<h2>'. $sec4_title .'</h2>';
                                }
                            ?>
                            <?php
                                $sec4_subtitle = get_field('sec4_subtitle');
                                if( !empty($sec4_subtitle) ) {
                                    echo '<p>'. $sec4_subtitle .'</p>';
                                }
                            ?>
                        </div>

                        <div class="row">

                            <?php
                                $sec4_text_1 = get_field('sec4_text_1');
                                if( !empty($sec4_text_1) ) { ?>
                                    <div class="col-lg-6 col-md-12 mb-lg-5">
                                        <div class="c-border-top f-md">
                                            <?= $sec4_text_1 ?>
                                        </div>
                                    </div> <!-- col -->
                            <?php } ?>
                            <?php
                                $sec4_text_2 = get_field('sec4_text_2');
                                if( !empty($sec4_text_2) ) { ?>
                                    <div class="col-lg-6 col-md-12 mb-lg-5">
                                        <div class="c-border-top f-md">
                                            <?= $sec4_text_2 ?>
                                        </div>
                                    </div> <!-- col -->
                            <?php } ?>
                            <?php
                                $sec4_text_3 = get_field('sec4_text_3');
                                if( !empty($sec4_text_3) ) { ?>
                                    <div class="col-lg-6 col-md-12 mb-lg-5">
                                        <div class="c-border-top f-md">
                                            <?= $sec4_text_3 ?>
                                        </div>
                                    </div> <!-- col -->
                            <?php } ?>
                            <?php
                                $sec4_text_4 = get_field('sec4_text_4');
                                if( !empty($sec4_text_4) ) { ?>
                                    <div class="col-lg-6 col-md-12 mb-lg-5">
                                        <div class="c-border-top f-md">
                                            <?= $sec4_text_4 ?>
                                        </div>
                                    </div> <!-- col -->
                            <?php } ?>

                        </div> <!-- row -->

                    </div>  <!-- col -->
                </div> <!-- row -->
            </div> <!-- col -->
        </div> <!-- row -->  

        <div class="row mt-lg-5 mt-md-3 pt-5">
            <div class="col-lg-12 mb-4 text-center">
                <?php
                    $sec5_title = get_field('sec5_title');
                    if( !empty($sec5_title) ) {
                        echo '<h2>'. $sec5_title .'</h2>';
                    }
                ?>
                <?php
                    $sec5_subtitle = get_field('sec5_subtitle');
                    if( !empty($sec5_subtitle) ) {
                        echo '<p>'. $sec5_subtitle .'</p>';
                    }
                ?>
            </div> <!-- col -->
            <div class="col-md-6 mb-5 mb-sm-5 mb-md-0">
                <?php 
                    $sec5_image_left_side = get_field('sec5_image_left_side');
                    if( !empty($sec5_image_left_side) ): ?>
                        <img src="<?= $sec5_image_left_side['url']; ?>"  alt="<?= $sec5_image_left_side['alt']; ?>"
                        class="shadow-img mb-2"/>
                <?php endif; ?>
                <div class="row mt-3 mt-md-4">
                    <?php
                        $sec5_text_left_1 = get_field('sec5_text_left_1');
                        if( !empty($sec5_text_left_1) ) { ?>
                            <div class="col-lg-6 col-md-12 col-sm-6">
                                <div class="c-border-top f-md">
                                    <?= $sec5_text_left_1 ?>
                                </div>
                            </div> <!-- col -->
                    <?php } ?>
                    <?php
                        $sec5_text_left_2 = get_field('sec5_text_left_2');
                        if( !empty($sec5_text_left_2) ) { ?>
                            <div class="col-lg-6 col-md-12 col-sm-6">
                                <div class="c-border-top f-md">
                                    <?= $sec5_text_left_2 ?>
                                </div>
                            </div> <!-- col -->
                    <?php } ?>
                </div> <!-- row -->
            </div> <!-- col -->
            <div class="col-md-6">
                <?php 
                    $sec5_image_right_side = get_field('sec5_image_right_side');
                    if( !empty($sec5_image_right_side) ): ?>
                        <img src="<?= $sec5_image_right_side['url']; ?>"  alt="<?= $sec5_image_right_side['alt']; ?>"
                        class="shadow-img mb-2"/>
                <?php endif; ?>
                <div class="row mt-3 mt-md-4">
                    <?php
                        $sec5_text_right_1 = get_field('sec5_text_right_1');
                        if( !empty($sec5_text_right_1) ) { ?>
                            <div class="col-lg-6 col-md-12 col-sm-6">
                                <div class="c-border-top f-md">
                                    <?= $sec5_text_right_1 ?>
                                </div>
                            </div> <!-- col -->
                    <?php } ?>
                    <?php
                        $sec5_text_right_2 = get_field('sec5_text_right_2');
                        if( !empty($sec5_text_right_2) ) { ?>
                            <div class="col-lg-6 col-md-12 col-sm-6">
                                <div class="c-border-top f-md">
                                    <?= $sec5_text_right_2 ?>
                                </div>
                            </div> <!-- col -->
                    <?php } ?>
                </div> <!-- row -->
            </div> <!-- col -->
        </div> <!-- row -->  
    </div> <!-- container -->
</div> <!-- inner page wrap -->



<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action', 'page' ); ?>
<!-- Call to Action Section END -->

<?php get_footer();
