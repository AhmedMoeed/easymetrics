<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package EasyMetrics
 */

get_header();
?>

<div class="innerpage_wrap">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 col-lg-8">
				<div class="row">
					<?php if ( have_posts() ) : ?>
						<div class="col-12">
							<header class="page-header">
								<h3 class="page-title">
									<?php
									/* translators: %s: search query. */
									printf( esc_html__( 'Search Results for: %s', 'easymetrics' ), '<span>' . get_search_query() . '</span>' );
									?>
								</h3>
							</header><!-- .page-header -->
							<hr />
						</div>
						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							if (is_search() && ($post->post_type !=='post')) continue;
								$thumbnail_URL = get_the_post_thumbnail_url();
								$post_categories = wp_get_post_categories( get_the_ID());
							/**
							 * Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called content-search.php and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'search' );

						endwhile;

						// the_posts_navigation();

					else :
						get_template_part( 'template-parts/content', 'none' );
						endif; ?>
					</div>
				</div> <!-- col 8 -->
				<div class="col-lg-4">
					<?php get_sidebar('blog'); ?>
				</div> <!-- col 4 -->
			</div> <!-- col 8 -->	
		</div> <!-- row -->
	</div> <!-- container -->
</div>  <!-- innerpage_wrap -->

<?php
get_footer();
