<?php
/**
 * template name: temp-news
 */

get_header();
?>

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/inner-header', 'page' ); ?>
<!-- Call to Action Section END -->


<!-- Industries Listing Start -->
<section class="section s-feature">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 main_left_side">

				<?php 
					$args = array(
						'post_type' 		=> 'news', 
						'posts_per_page' 	=> -1,
						'post_status'		=> 'publish'
					); 
					$solutions = new WP_Query($args); 
				?>
				<?php 

					while ( $solutions->have_posts() ) : $solutions->the_post(); 
					?>
						<article class="news-article">
							<h3><a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h3>
							<div class="article-meta">
								<?php echo get_the_date( 'M j, Y' );  ?>
							</div>
							<div class="article-image">
								<a href="<?php the_permalink(); ?>"> 
									<?php echo get_the_post_thumbnail(); ?>
								</a>
							</div>
							<div class="article-actions">
								<a class="read_more d-inline-flex align-items-center" href="<?php the_permalink(); ?>"> Full Article <span class="arrow-right"></span> </a>
							</div>
						</article>
					<?php  
					endwhile;  
				wp_reset_query();
				// end of the loop. ?>
			
			</div> <!-- col 9 -->
			<div class="col-lg-4 main_right_side">
				<?php get_sidebar(); ?>
			</div> <!-- col 4 -->
		</div> <!-- row -->		
	</div> <!-- container -->
</section> <!-- section -->
<!-- Industries Listing END -->
 
<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action', 'page' ); ?>
<!-- Call to Action Section END -->

<?php get_footer();
