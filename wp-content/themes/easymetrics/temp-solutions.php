<?php
/**
 * template name: temp-solutions
 */

get_header();
?>

<!-- Hero Section Start-->
<?php 
	$hero_bg_image = get_field('h_b_image');
	$hero_title =get_field('h_title');
	$hero_content =get_field('h_text_content');
?>
<section class="hero" <?php if($hero_bg_image){?> style="background-image: url('<?= $hero_bg_image; ?>')" <?php } ?> >
	<div class="container text-center">
		<div class="row">
			<div class="col-lg-7 col-md-10 mx-auto">
				<div class="mb-4">
					<h2><?php echo $hero_title; ?></h2>
				</div>
				<div class="text-content text-white">
					<?php echo $hero_content; ?>
				</div>
			</div> <!-- col -->
		</div> <!-- row -->
	</div> <!-- container -->
</section> <!-- section -->
<!-- Hero Section END -->

<!-- Industries Listing Start -->
<section class="section s-feature">
	<div class="container">
		<div class="row">
			<div class="col-md-11 mx-auto">

			<?php 
				$i = 1;
				$args = array(
					'post_type' 		=> 'solutions', 
					'posts_per_page' 	=> -1,
					'post_status'		=> 'publish'
				); 
				$solutions = new WP_Query($args); 
			?>
			<?php
				while ( $solutions->have_posts() ) : $solutions->the_post(); 
				$thumbnail = get_field('industries_listing_thumbnail');
				?>
					<?php
						if( ($i % 2) == 0 ) {
							echo '<div class="align-items-center row mb-5 flex-row-reverse">';
						} else {
							echo '<div class="align-items-center row mb-5">';
						}
					?>
						<div class="col-lg-6 col-md-12 mb-3 mb-lg-0">
							<h2><?php the_title(); ?></h2>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink(); ?>" class="btn btn-info btn-sm mt-4"> Read More</a>
						</div>
						<div class="col-lg-6 col-md-12">
							<?php
								if( !empty($thumbnail) ): ?>
								<img src="<?php echo $thumbnail['url']; ?>" 
								alt="<?php echo $thumbnail['alt']; ?>" />
							<?php endif; ?>
						</div>
					</div> <!-- row -->

				<?php 
				$i++;
				endwhile;  
			wp_reset_query();
			// end of the loop. ?>
			
			</div> <!-- col 10 -->
		</div> <!-- row -->		
	</div> <!-- container -->
</section> <!-- section -->
<!-- Industries Listing END -->

<?php the_content(); ?>

<!-- Industries Section Start --> 
<section class="section industries-list">
	<div class="container">
		<?php
			$sec2_title = get_field('sec2_title');
			if( !empty($sec2_title) ) {
				echo '<h2 class="text-center mb-4">'. $sec2_title .'</h2>';
			}
		?>
		<?php $industries_list = get_field('industries_listing');
			if( $industries_list ): ?>
				<div class="row">
					<?php foreach( $industries_list as $p ): 
						$icon = get_field('industry_thumbnail_icon', $p->ID );
						?>
						<div class="col-md-3 col-sm-6">
							<div class="industry">
								<a href="<?php echo get_permalink( $p->ID ); ?>">
									<span class="icon">
									<?php
									if( !empty($icon) ): ?>
										<img src="<?php echo $icon['url']; ?>" 
										alt="<?php echo $icon['alt']; ?>" />
									<?php endif; ?>
									</span>
									<span class="name"><?php echo get_the_title( $p->ID ); ?></span>
								</a>
							</div>
						</div> <!-- col -->
					<?php endforeach; ?>
				</div>
		<?php endif; ?>
	</div> <!-- container -->	
</section>
<!-- Industries Section END --> 

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action', 'page' ); ?>
<!-- Call to Action Section END -->

<?php get_footer();
