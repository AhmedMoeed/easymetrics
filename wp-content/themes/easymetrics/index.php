<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Forge Saas
 */

get_header(); ?>

<div class="inner-page-header">
	<div class="container">
		<h1> Blog </h1>
	</div> <!-- container -->
</div> <!-- inner page header -->


<div class="innerpage_wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 main_left_side">
				<div class="row" id="blogContainerWrapper">
					<?php
					$i = 0;

					$args = array( 
						'post_type' 		=> 'post', 
						'posts_per_page' 	=> 8, 
						'post_status'		=> 'publish'
					);

					$postslist = new WP_Query( $args );
					
					if ( $postslist->have_posts() ) :
						while ( $postslist->have_posts() ) : $postslist->the_post(); 
							$thumbnail_URL = get_the_post_thumbnail_url();
							$post_categories = wp_get_post_categories( get_the_ID());
						?>
							<div class="col-md-6">
								<div class="blog-post">
									<a href="<?php the_permalink() ?>" class="link"></a>
									<?php echo get_the_post_thumbnail(); ?>
									<div class="blog-content">
										<div class="category">
											<?= get_category($post_categories[0])->name; ?>
										</div>
										<div class="text-content">
											<h4><?php the_title(); ?></h4>
										</div>
										<div class="meta-info">
											<?php 
												echo get_the_date( 'M j, Y' ); 
												$author = get_the_author_meta('display_name');
												if($author) { echo ' |  By : ' . $author; }
											?>
										</div>
									</div>
								</div>
							</div> <!-- col 6 -->
						<?php  endwhile; 
						wp_reset_postdata();
						endif;
					?>
				</div> <!-- row -->
				<div class="loadingposts"> More Posts Loading </div>
			</div> <!-- col 8 -->
			<div class="col-lg-4 main_right_side">
				<?php get_sidebar('blog'); ?>
			</div> <!-- col 4 -->
		</div> <!-- row -->
	</div> <!-- container -->
</div>  <!-- innerpage_wrap -->

<?php get_footer(); ?>

<script>
    let Offsetposts = 8;
	
	let canBeLoaded = true,  bottomOffset = 2000;
	// let ajaxurl = "http://localhost/easymetrics/wp-admin/admin-ajax.php";
	let ajaxurl = "/wp-admin/admin-ajax.php";
	$(window).scroll(function() {
		let data = {
			'dataType': 'html',
			'action': 'blogposts',
			'security': '<?php echo wp_create_nonce("load_more_posts"); ?>',
			'offsetPosts': Offsetposts
		};
	    if( $(document).scrollTop() > ( $(document).height() - bottomOffset ) && canBeLoaded == true ){
	        $.ajax({
				url  : ajaxurl,
				data : data,
				type : 'POST',
				beforeSend: function( xhr ) {
					// you can also add your own preloader here
					// you see, the AJAX call is in process, we shouldn't run it again until complete					
					canBeLoaded = false;
				},
				success:function(data) {
					console.log( data );
					console.log( typeof data );
					if( data !== '0' ) {
						$('.loadingposts').show();
						jQuery('#blogContainerWrapper').append( data ); // where to insert posts
						jQuery('.loadingposts').hide();
						canBeLoaded = true; // the ajax is completed, now we can run it again
						Offsetposts += 8;
					} else {
						$('.loadingposts').hide();
					}
				}
			});
	    }
    });
</script>