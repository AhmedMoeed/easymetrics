<?php
/**
 * template name: temp-integration
 */

get_header();
?>

<section class="hero">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-9">
				<?php
					$sec1_title = get_field('sec1_title');
					if( !empty($sec1_title) ) {
						echo '<div class="mb-4">';
							echo '<h2>'. $sec1_title .'</h2>';
						echo '</div>';
					}
                ?>
                <?php
					$sec1_textcontent = get_field('sec1_textcontent');
					if( !empty($sec1_textcontent) ) {
						echo '<div class="text-content large-text text-white">';
						echo $sec1_textcontent;
						echo '</div>';
					}
				?>
			</div> <!-- col -->
		</div> <!-- row -->
	</div> <!-- container -->
</section> <!-- section -->

<div class="innerpage_wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<div class="page-main-content-area">
					<?php the_content(); ?>
				</div>
			</div> <!-- col 8 -->
			<div class="col-lg-4">
				<?php get_sidebar(); ?>
			</div> <!-- col 4 -->
		</div><!-- row -->
	</div> <!-- container -->
</div> <!-- innerpage_wrap -->

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action', 'page' ); ?>
<!-- Call to Action Section END -->

<?php get_footer();
