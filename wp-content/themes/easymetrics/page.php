<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package EasyMetrics
 */

get_header();
?>
<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/inner-header'); ?>
<!-- Call to Action Section END -->

<div class="innerpage_wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 main_left_side">
				<div class="single-page-body">
					<div class="page-main-content-area">
						<?php
							while ( have_posts() ) : the_post();
								get_template_part( 'template-parts/content', 'page' );
							endwhile; // End of the loop.
						?>
					</div>
				</div>
			</div> <!-- col 8 -->
			<div class="col-lg-4 main_right_side">
				<?php get_sidebar(); ?>
			</div> <!-- col 4 -->
		</div> <!-- row -->
	</div> <!-- container -->
</div>


<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action'); ?>
<!-- Call to Action Section END -->

<?php get_footer();
