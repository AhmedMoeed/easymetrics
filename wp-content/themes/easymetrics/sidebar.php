<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EasyMetrics
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="sidebar">
	<div class="widget requestdemowidget">
		<div class="row align-items-center">
			<?php 
				$request_demo_title = get_field('request_demo_title', 'option');
				if( !empty($request_demo_title) ) {?>
					<div class="col-md-8">
						<h4 class="widget-title"><?= $request_demo_title; ?></h4>
					</div>
			<?php } ?>
			<?php 
				$request_demo_icon = get_field('request_demo_icon', 'option');
				if( !empty($request_demo_icon) ): ?>
					<div class="col-md-4">
						<div class="icon">
							<img src="<?= $request_demo_icon['url']; ?>" alt="<?= $request_demo_icon['alt']; ?>" />
						</div>
					</div>
				<?php endif; ?>
		</div> <!-- row -->
		<?php
			$request_demo_desc = get_field('request_demo_desc', 'option');
			if( !empty($request_demo_desc) ) {?>
				<div class="text-content">
					<?= $request_demo_desc; ?>
				</div>
		<?php } ?>
		<?php
			$request_demo_btn_text = get_field('request_demo_btn_text', 'option');
			$request_demo_btn_URL = get_field('request_demo_btn_URL', 'option');
			if( !empty($request_demo_btn_text) ) { ?>
				<a href="<?= $request_demo_btn_URL; ?>" class="btn btn-block btn-primary"> 
					<?= $request_demo_btn_text; ?>
				</a>
		<?php } ?>
	</div> <!-- widget request demo -->

	<div class="widget requestdemowidget">
		<div class="row align-items-center">
			<?php 
				$download_guide_title = get_field('download_guide_title', 'option');
				if( !empty($download_guide_title) ) {?>
					<div class="col-md-8">
						<h4 class="widget-title"><?= $download_guide_title; ?></h4>
					</div>
			<?php } ?>
			<?php 
				$download_guide_icon = get_field('download_guide_icon', 'option');
				if( !empty($download_guide_icon) ): ?>
					<div class="col-md-4">
						<div class="icon">
							<img src="<?= $download_guide_icon['url']; ?>" alt="<?= $download_guide_icon['alt']; ?>" />
						</div>
					</div>
				<?php endif; ?>
		</div> <!-- row -->
		<?php
			$download_guide_desc = get_field('download_guide_desc', 'option');
			if( !empty($download_guide_desc) ) {?>
				<div class="text-content">
					<?= $download_guide_desc; ?>
				</div>
		<?php } ?>
		<?php
			$download_guide_link_text = get_field('download_guide_link_text', 'option');
			$download_guide_link_URL = get_field('download_guide_link_URL', 'option');
			if( !empty($download_guide_link_text) ) { ?>
				<a href="<?= $download_guide_link_URL; ?>" target="blank" class="guide-note"> 
					<?= $download_guide_link_text; ?>
				</a>
		<?php } ?>
	</div> <!-- widget request demo -->
</div>
