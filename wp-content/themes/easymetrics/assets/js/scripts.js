const mobileOpenBtn = document.querySelector('.mobile-button');
mobileOpenBtn.addEventListener('click', function(){
	document.querySelector('body').classList.toggle('openMenu');
	this.classList.toggle('open');
});



$(document).ready( function() {
	// This will make the cookie bar disappear when the accept button is clicked
	jQuery('.cookies-accept-btn').click(function(){
		jQuery(this).parents('.cookies-accept').fadeOut('slow');   
		$('body').css({
			'padding-bottom': '0px'
		});     
		if (!jQuery('.cookies-accept').is()) {       
			// Add the cookie. It will remain for a year
			jQuery.cookie('cookiebar', 1, {expires: 365, path: '/'});
			}
		return false;
	});

	// If the cookie is true, don't show the cookie bar
	if (!jQuery.cookie('cookiebar') == 1) {
		jQuery('.cookies-accept').css({'display':'block'});
		$('body').css({
			'padding-bottom': '50px'
		});
	}
});