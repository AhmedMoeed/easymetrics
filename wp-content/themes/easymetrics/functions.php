<?php
/**
 * EasyMetrics functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package EasyMetrics
 */

if ( ! function_exists( 'easymetrics_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function easymetrics_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on EasyMetrics, use a find and replace
		 * to change 'easymetrics' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'easymetrics', get_template_directory() . '/languages' );
		
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-header' 		=> esc_html__( 'Primary', 'easymetrics' ),
			'menu-footer1' 	  	=> esc_html__( 'Footer 1', 'easymetrics' ),
			'menu-footer2' 	  	=> esc_html__( 'Footer 2', 'easymetrics' ),
			'menu-footer3' 	  	=> esc_html__( 'Footer 3', 'easymetrics' ),
			'menu-footer4' 	  	=> esc_html__( 'Footer 4', 'easymetrics' ),
			'menu-copyright' 	=> esc_html__( 'Copyright Menu', 'easymetrics' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'easymetrics_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
		
		wp_oembed_add_provider( '/https?:\/\/(.+)?(wistia.com|wi.st)\/(medias|embed)\/.*/', 'http://fast.wistia.com/oembed', true);
	}
endif;
add_action( 'after_setup_theme', 'easymetrics_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function easymetrics_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'easymetrics_content_width', 640 );
}
add_action( 'after_setup_theme', 'easymetrics_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function easymetrics_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'easymetrics' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'easymetrics' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'easymetrics_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function easymetrics_scripts() {
	wp_enqueue_style( 'easymetrics-style', get_stylesheet_uri() );

	wp_enqueue_style( 'firasans', 'https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,500,600,700,800,900');
	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/style/vendors/all.css');
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/style/vendors/bootstrap.min.css');
	wp_enqueue_style( 'sofiaProFont', 'https://use.typekit.net/jnq3uxd.css');
	wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/assets/style/custom-style/style.css');
	
	
	wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/assets/js/jquery-3.4.1.min.js', array(), '20151215', true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), '20151215', true );
	wp_enqueue_script( 'jquery-cookie', get_template_directory_uri() . '/assets/js/jquery.cookie.js', array(), '20151215', true );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.js', array(), '20151215', true );

	wp_enqueue_script( 'easymetrics-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'easymetrics-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'easymetrics_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

require get_template_directory() . '/inc/custom-post-types.php';

/**
 * Add new ACF options pages
 */ 
if(function_exists("register_options_page"))
{
	register_options_page('Theme Options');	
}

add_post_type_support('page', 'excerpt');

add_action('wp_ajax_blogposts', 'loadBlogPosts');
add_action('wp_ajax_nopriv_blogposts', 'loadBlogPosts');

function loadBlogPosts() {
	$offsetPosts = $_REQUEST['offsetPosts'];
	$emprray = array();
	$args = array(
	    'post_type' 		=> 'post',
	    'order' 			=> 'DESC',
	    'post_status' 		=> 'publish',
	    'posts_per_page'	=> 8,
	    'offset'            => $offsetPosts
	);
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		
        while ( $query->have_posts() ) { 
			$query->the_post();
				$thumbnail_URL = get_the_post_thumbnail_url();
				$post_categories = wp_get_post_categories( get_the_ID());
		?>
		<div class="col-md-6">
			<div class="blog-post">
				<a href="<?php the_permalink() ?>" class="link"></a>
				<?php echo get_the_post_thumbnail(); ?>
				<div class="blog-content">
					<div class="category">
						<?= get_category($post_categories[0])->name; ?>
					</div>
					<div class="text-content">
						<h4><?php the_title(); ?></h4>
					</div>
					<div class="meta-info">
						<?php 
							echo get_the_date( 'M j, Y' ); 
							$author = get_the_author_meta('display_name');
							if($author) { echo ' |  By : ' . $author; }
						?>
					</div>
				</div>
			</div>
		</div> <!-- col 6 -->
	<?php
        }
	} else {
		return;
	}
	wp_die();
}