<?php
/**
 * template name: temp-demoRequest
 */

get_header();
?>	
<div class="container">
	<div class="requestdemo-content-container">	
		<div class="row">
			<div class="col-lg-5">
				<?php the_content(); ?>
			</div> <!-- col -->
			<div class="col-lg-6 offset-lg-1">
				<div class="requestdemo-form">
					<?php echo do_shortcode('[contact-form-7 id="2086" title="requestDemo Form"]'); ?>
				</div> 
			</div> <!-- col -->
		</div> <!-- row -->
	</div> <!-- contactpage-wrap -->
</div> <!-- container -->

<?php get_footer();
