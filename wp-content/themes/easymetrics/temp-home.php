<?php
/**
 * template name: temp-home
 */

get_header();
?>

<section class="hero">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-xl-5 col-lg-6 col-md-9 col-sm-12">
				<?php
					$sec1_title = get_field('sec1_title');
					if( !empty($sec1_title) ) {
						echo '<h2>'. $sec1_title .'</h2>';
					}
				?>
				<?php
					$sec1_textcontent = get_field('sec1_text_content');
					if( !empty($sec1_textcontent) ) {?>
						<div class="h-content">
							<?php echo $sec1_textcontent; ?>
						</div>
				<?php } ?>
				<?php
					$sec1_btn_1_text = get_field('sec1_btn_1_text');
					$sec1_btn_1_URL = get_field('sec1_btn_1_URL');
					if( !empty($sec1_btn_1_text) ) { ?>
						<a href="<?= $sec1_btn_1_URL; ?>" class="btn btn-primary"> <?= $sec1_btn_1_text; ?> </a>
				<?php } ?>
				<?php
					$sec1_btn_2_text = get_field('sec1_btn_2_text');
					$sec1_btn_2_URL = get_field('sec1_btn_2_URL');
					if( !empty($sec1_btn_1_text) ) { ?>
						<a href="<?= $sec1_btn_2_URL; ?>" class="btn btn-outline-light"> 
							<?= $sec1_btn_2_text; ?> 
						</a>
				<?php } ?>
			</div> <!-- col -->
			<div class="col-lg-6 d-none d-lg-block offset-xl-1">
				<?php 
					$sec1_image = get_field('sec1_image');
					if( !empty($sec1_image) ): ?>
						<div class="hero-img">
							<img src="<?= $sec1_image['url']; ?>"  alt="<?= $sec1_image['alt']; ?>" />
						</div>
				<?php endif; ?>
			</div>
		</div><!-- row -->
	</div> <!-- container -->
</section> <!-- section -->

<section class="home-customers">
	<div class="container">
		<?php if( have_rows('sec2_logo') ): ?>
			<div class="row align-items-center">
				<?php while( have_rows('sec2_logo') ): the_row(); 
					$image = get_sub_field('sec2_logo_image');
				?>
					<div class="col-4 col-lg-2">
						<?php if( $image ): ?>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div> <!-- row -->
		<?php endif; ?>
	</div> <!-- container -->
</section> <!-- section -->

<section class="home-features">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="head">
					<?php
						$sec3_title = get_field('sec3_title');
						if( !empty($sec3_title) ) {
							echo '<h2>'. $sec3_title .'</h2>';
						}
					?>
					<?php
						$sec3_text_content = get_field('sec3_text_content');
						if( !empty($sec3_text_content) ) {?>
							<div class="textContent d-none d-md-block">
								<?php echo $sec3_text_content; ?>
							</div>
					<?php } ?>
				</div>
			</div> <!-- column -->
		</div> <!-- row -->

		<?php if( have_rows('sec3_columns') ): ?>
			<div class="row">
				<?php while( have_rows('sec3_columns') ): the_row(); 
					$sec3_col_logo = get_sub_field('sec3_col_logo');
					$sec3_col_title = get_sub_field('sec3_col_title');
					$sec3_col_short_desc = get_sub_field('sec3_col_short_desc');
					$sec3_col_button_text = get_sub_field('sec3_col_button_text');
					$sec3_col_button_url = get_sub_field('sec3_col_button_url');
				?>
					<div class="col-md-4">
						<div class="d-md-block d-flex h-feature">
							<?php if( $sec3_col_logo ): ?>
								<div class="icon">
									<img src="<?php echo $sec3_col_logo['url']; ?>" alt="<?php echo $sec3_col_logo['alt'] ?>" />
								</div>
							<?php endif; ?>
							<div class="content-wrap">
								<?php
									if( !empty($sec3_col_title) ) { ?>
										<div class="title">
											<h4> <?= $sec3_col_title ?> </h4>
										</div>
								<?php } ?>
								<?php
									if( !empty($sec3_col_short_desc) ) {?>
										<div class="textContent">
											<?= $sec3_col_short_desc; ?>
										</div>
								<?php } ?>
								<?php if( !empty($sec3_col_button_text) ) { ?>
									<div class="resource-btn">
										<a href="<?= $sec3_col_button_url; ?>" class="btn btn-block btn-primary"> 
											<?= $sec3_col_button_text; ?> 
										</a>
									</div>
								<?php } ?>
							</div>
						</div>
					</div> <!-- column -->
				<?php endwhile; ?>
			</div> <!-- row -->
		<?php endif; ?>
	</div> <!-- container -->
</section> <!-- section -->

<section class="home-integration">
	<div class="container">
		<div class="section-head">
			<?php
				$sec4_title = get_field('sec4_title');
				if( !empty($sec4_title) ) {
					echo '<h2>'. $sec4_title .'</h2>';
				}
			?>
			<?php
				$sec4_text_content = get_field('sec4_text_content');
				if( !empty($sec4_text_content) ) {?>
					<div class="text-content">
						<?php echo $sec4_text_content; ?>
					</div>
			<?php } ?>

			<?php 
			$sec4_btn_text = get_field('sec4_btn_text');
			$sec4_btn_URL = get_field('sec4_btn_URL');
			if( !empty($sec4_btn_text) ) { ?>
				<a href="<?= $sec4_btn_URL; ?>" class="btn btn-outline-primary">
					<?= $sec4_btn_text; ?>
				</a>
			<?php } ?>
		</div>
		<div class="flow-cart">
			<?php  $sec4_image_desktop = get_field('sec4_image_desktop');
			if( $sec4_image_desktop ): ?>
				<div class="d-none d-md-block">
					<img src="<?php echo $sec4_image_desktop['url']; ?>" alt="<?php echo $sec4_image_desktop['alt'] ?>" />
				</div>
			<?php endif; ?>

			<?php  $sec4_image_mobile = get_field('sec4_image_mobile');
			if( $sec4_image_mobile ): ?>
				<div class="d-md-none d-sm-block">
					<img 
						src="<?php echo $sec4_image_mobile['url']; ?>" 
						alt="<?php echo $sec4_image_mobile['alt'] ?>" 
						class="img-fluid img-mobile"
						/>
				</div>
			<?php endif; ?>
		</div>
	</div> <!-- container -->
</section> <!-- section -->

<section class="home-testimonials">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="textContent">
					<?php
						$sec5_title = get_field('sec5_title');
						if( !empty($sec5_title) ) {
							echo '<h2>'. $sec5_title .'</h2>';
						}
					?>
					<?php
						$sec5_text_content = get_field('sec5_text_content');
						if( !empty($sec5_text_content) ) {?>
							<div class="d-none d-md-block">
								<?php echo $sec5_text_content; ?>
							</div>
					<?php } ?>				
				</div>
			</div> <!-- column -->
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-12 col-md-6">
						<div class="user-quote d-lg-flex align-items-center">
							<?php $sec5_supervisor_image = get_field('sec5_supervisor_image');
							if( $sec5_supervisor_image ): ?>
								<div class="image d-none d-md-block">
									<img 
										src="<?php echo $sec5_supervisor_image['url']; ?>" 
										alt="<?php echo $sec5_supervisor_image['alt'] ?>" 
										/>
								</div>
							<?php endif; ?>
							<div>
								<?php
									$sec5_supervisor_title = get_field('sec5_supervisor_title');
									if( !empty($sec5_supervisor_title) ) {
										echo '<h5>'. $sec5_supervisor_title .'</h5>';
									}
								?>
								<?php 
								$sec5_supervisor_btn_text = get_field('sec5_supervisor_btn_text');
								$sec5_supervisor_btn_URL = get_field('sec5_supervisor_btn_URL');
								if( !empty($sec5_supervisor_btn_text) ) { ?>
									<a href="<?= $sec5_supervisor_btn_URL; ?>" class="btn btn-outline-primary">
										<span class="i-play"></span>
										<?= $sec5_supervisor_btn_text; ?>
									</a>
								<?php } ?>
							</div>
						</div> <!-- user quote -->
					</div> <!-- column -->
					<div class="col-lg-12 col-md-6">
						<div class="user-quote d-lg-flex align-items-center">
							<?php $sec5_executives_image = get_field('sec5_executives_image');
								if( $sec5_executives_image ): ?>
									<div class="image d-none d-md-block">
										<img 
											src="<?php echo $sec5_executives_image['url']; ?>" 
											alt="<?php echo $sec5_executives_image['alt'] ?>" 
											/>
									</div>
							<?php endif; ?>
							<div>
								<?php
									$sec5_executives_title = get_field('sec5_executives_title');
									if( !empty($sec5_executives_title) ) {
										echo '<h5>'. $sec5_executives_title .'</h5>';
									}
								?>
								<?php 
								$sec5_executives_btn_text = get_field('sec5_executives_btn_text');
								$sec5_executives_btn_URL = get_field('sec5_executives_btn_URL');
								if( !empty($sec5_executives_btn_text) ) { ?>
									<a href="<?= $sec5_executives_btn_URL; ?>" class="btn btn-outline-primary">
										<span class="i-play"></span>
										<?= $sec5_executives_btn_text; ?>
									</a>
								<?php } ?>
							</div>
						</div> <!-- user quote -->
					</div> <!-- column -->
				</div> <!-- row -->
			</div> <!-- column -->
		</div> <!-- row -->

		<?php if( have_rows('sec5_testimonial') ): ?>
			<div class="row d-none d-md-flex">
				<?php while( have_rows('sec5_testimonial') ): the_row(); 
					$sec5_t_logo = get_sub_field('sec5_t_logo');
					$sec5_t_title = get_sub_field('sec5_t_title');
					$sec5_short_desc = get_sub_field('sec5_short_desc');
				?>
					<div class="col-md-6">
						<div class="quote">
							<?php
								if( !empty($sec5_short_desc) ) {?>
									<div class="q-text">
										<?= $sec5_short_desc; ?>
									</div>
							<?php } ?>
							<div class="q-info">
								<?php
									if( !empty($sec5_t_title) ) { ?>
										<h3> <?= $sec5_t_title ?> </h3>
								<?php } ?>
								<?php if( $sec5_t_logo ): ?>
									<div class="q-logo">
										<img src="<?php echo $sec5_t_logo['url']; ?>" alt="<?php echo $sec5_t_logo['alt'] ?>" />
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div> <!-- column -->
				<?php endwhile; ?>
			</div> <!-- row -->
		<?php endif; ?>
	</div> <!-- container -->
</section> <!-- section -->


<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action'); ?>
<!-- Call to Action Section END -->

<section class="blog-section">
	<div class="container">
		<?php
			$sec6_title = get_field('sec6_title');
			if( !empty($sec6_title) ) { ?>
				<div class="head">
					<?php echo '<h2>'. $sec6_title .'</h2>'; ?>
				</div>
		<?php } ?>

		<?php 

		$blog_posts = get_field('sec6_blog_post');
		if( $blog_posts ): ?>
			<div class="row">
				<?php foreach( $blog_posts as $post): // variable must be called $post (IMPORTANT) ?>
					<?php setup_postdata($post); ?>
					<div class="col-lg-4">
						<div class="h-blog-item">
							<div class="b-title">
								<h3><?php the_title(); ?></h3>
							</div>
							<div class="b-content">
								<p><?php the_excerpt(); ?> </p>
							</div>
							<div class="resource-btn">
								<a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More</a>
							</div> 
						</div>
					</div> <!-- column -->
				<?php endforeach; ?>
			</div>
			<?php wp_reset_postdata(); ?>
		<?php endif; ?>
	</div> <!-- container -->
</section> <!-- section -->

<?php get_footer();
