<?php
/**
 * template name: leadership
 */

get_header();
?>

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/inner-header', 'page' ); ?>
<!-- Call to Action Section END -->

<div class="innerpage_wrap">
	<div class="container">
		<div class="row">
			<?php
				$i = 1;
				$people_args = array(
					'posts_per_page' => -1, 
					'post_type' => 'people'
				); 
				$people = new WP_Query($people_args);
				while ( $people->have_posts() ) : $people->the_post(); 
				$image = get_the_post_thumbnail_url();
			?>

				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="team-member" data-toggle="modal" data-target="#leader<?=$i;?>">
						<div class="image">
							<img src="<?= get_template_directory_uri();?>/assets/images/placeholder-image.png" alt="<?php the_title(); ?>"/>
							<div class="image-leader" style="background-image: url(<?= $image; ?>)"></div>
						</div>
						<h3><?php the_title(); ?></h3>
						<div class="designation"><?php the_field('title'); ?></div>
					</div>
				</div> <!-- col -->

			<?php 
			$i++;
			endwhile; 
			wp_reset_postdata();
			// end of the loop. ?>

		</div><!-- row -->
	</div> <!-- container -->
</div> <!-- innerpage_wrap -->

<?php echo the_content(); ?>

<?php 
	$i = 1;
	$people_args = array(
		'posts_per_page' => -1, 
		'post_type' => 'people'
	); 
	$people = new WP_Query($people_args);
	while ( $people->have_posts() ) : $people->the_post(); 
?>

<!-- Modal -->
<div class="modal fade leaderPopup" id="leader<?=$i;?>" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
	  	<?php the_content(); ?>
      </div>
    </div>
  </div>
</div>

<?php 
$i++;
endwhile; // end of the loop. ?>

<!-- Call to Action Section Start --> 
<?php  get_template_part( 'template-parts/call-to-action', 'page' ); ?>
<!-- Call to Action Section END -->

<?php get_footer();
