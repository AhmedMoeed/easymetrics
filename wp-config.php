<?php /* BEGIN KINSTA STAGING ENVIRONMENT */ ?>
<?php if ( !defined('KINSTA_DEV_ENV') ) { define('KINSTA_DEV_ENV', true); /* Kinsta staging - don't remove this line */ } ?>
<?php if ( !defined('JETPACK_STAGING_MODE') ) { define('JETPACK_STAGING_MODE', true); /* Kinsta staging - don't remove this line */ } ?>
<?php /* END KINSTA STAGING ENVIRONMENT */ ?>
<?php
# Database Configuration
define( 'DB_NAME', 'easymetrics' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', 'localhost' );
define( 'DB_HOST_SLAVE', 'localhost' );
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', 'utf8_unicode_ci');

$table_prefix = 'wp_';

# Security Salts, Keys, Etc
define('AUTH_KEY',         '1mo8a8jhejkeyjjca5cyqy0mj9wdcpovgi5jqdmabhbfkfnsyc5ehp99hsezxrai');
define('SECURE_AUTH_KEY',  'winrbfzktdxccynwbj0ggrxe6kh2etqbuwtklb8zjg7ut1mb2llpgogbcieseqk8');
define('LOGGED_IN_KEY',    'aumgq5hlcbhehmzxsjaplsvawipy7fgree2fwevwojvoa1j1uth9tahbkc6buyrs');
define('NONCE_KEY',        'bvzsy4yy7pe3ltzqlhslc4pxnmbs6iwoh4tfci1fqov0vnqoxhqcef11uzlpunzu');
define('AUTH_SALT',        '7imzpqwkuvb1f4fcm8zechfmqdvbn1gbxnb3n4iy6qbdde3akpzisg1famym5cdu');
define('SECURE_AUTH_SALT', 'rzmr1khqhupn7a0nmeaafqexixtlwnie3unfwhrwwhftx8bop22lld98ghvihljs');
define('LOGGED_IN_SALT',   'fgt5domxnvxufrk3nilr1diydn7igogtbiyty0uytnyp3jiudchdw6yurmnnjnmk');
define('NONCE_SALT',       'onsspivo0pesjkobwogf07iqmz5q2ixtscxlgylnv6upmlbwpmmbis6nlqm9rswa');

define('RELOCATE',false);

# Localized Language Stuff

define( 'WP_CACHE', TRUE );

define( 'WP_AUTO_UPDATE_CORE', false );

define( 'PWP_NAME', 'easymetrics' );

define( 'FS_METHOD', 'direct' );

define( 'FS_CHMOD_DIR', 0775 );

define( 'FS_CHMOD_FILE', 0664 );

define( 'PWP_ROOT_DIR', '/nas/wp' );

define( 'WPE_APIKEY', '9301ac1d69c6d7c5e3421f2907b614e57563e58d' );

define( 'WPE_CLUSTER_ID', '100844' );

define( 'WPE_CLUSTER_TYPE', 'pod' );

define( 'WPE_ISP', true );

define( 'WPE_BPOD', false );

define( 'WPE_RO_FILESYSTEM', false );

define( 'WPE_LARGEFS_BUCKET', 'largefs.wpengine' );

define( 'WPE_SFTP_PORT', 2222 );

define( 'WPE_LBMASTER_IP', '' );

define( 'WPE_CDN_DISABLE_ALLOWED', true );

define( 'DISALLOW_FILE_MODS', FALSE );

define( 'DISALLOW_FILE_EDIT', FALSE );

define( 'DISABLE_WP_CRON', false );

define( 'WPE_FORCE_SSL_LOGIN', false );

define( 'FORCE_SSL_LOGIN', false );

/*SSLSTART*/ if ( isset($_SERVER['HTTP_X_WPE_SSL']) && $_SERVER['HTTP_X_WPE_SSL'] ) $_SERVER['HTTPS'] = 'on'; /*SSLEND*/

define( 'WPE_EXTERNAL_URL', false );

define( 'WP_POST_REVISIONS', FALSE );

define( 'WPE_WHITELABEL', 'wpengine' );

define( 'WP_TURN_OFF_ADMIN_BAR', false );

define( 'WPE_BETA_TESTER', false );

umask(0002);

$wpe_cdn_uris=array ( );

$wpe_no_cdn_uris=array ( );

$wpe_content_regexs=array ( );

$wpe_all_domains=array ( 0 => 'easymetrics.wpengine.com', );

$wpe_varnish_servers=array ( 0 => 'pod-100844', );

$wpe_special_ips=array ( 0 => '104.199.121.221', );

$wpe_ec_servers=array ( );

$wpe_largefs=array ( );

$wpe_netdna_domains=array ( );

$wpe_netdna_domains_secure=array ( );

$wpe_netdna_push_domains=array ( );

$wpe_domain_mappings=array ( );

$memcached_servers=array ( );
define('WPLANG', '');

# WP Engine ID


# WP Engine Settings






define('AUTOMATIC_UPDATER_DISABLED', true);
//define('WP_DEBUG', false);
if ( isset($_GET['debug']) && $_GET['debug'] == 'true')
    define('WP_DEBUG', true);

# That's It. Pencils down
if ( !defined('ABSPATH') )

	define('ABSPATH', dirname(__FILE__) . '/');
require_once(ABSPATH . 'wp-settings.php');

$_wpe_preamble_path = null; if(false){}
